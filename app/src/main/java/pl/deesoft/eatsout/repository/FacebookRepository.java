package pl.deesoft.eatsout.repository;

import android.os.Bundle;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.event.FacebookAuthenticationRequestEvent;
import pl.deesoft.eatsout.event.FacebookProfileRequestEvent;
import pl.deesoft.eatsout.util.Constants;

/**
 * Created by Piotr Krawczyk on 08.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class FacebookRepository implements FacebookCallback<LoginResult> {

    public void onEvent(final FacebookProfileRequestEvent facebookProfileRequestEvent) {
        GraphRequest request = GraphRequest.newMeRequest(facebookProfileRequestEvent.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    Hawk.put(Constants.FACEBOOK_IMAGE, Constants.FACEBOOK_IMAGE_URL.replace("{ID}", object.getString("id")));
                    Hawk.put(Constants.FACEBOOK_FIRSTNAME, object.getString("first_name"));
                    Hawk.put(Constants.FACEBOOK_LASTNAME, object.getString("last_name"));
                    EventBus.getDefault().post(new FacebookAuthenticationRequestEvent(facebookProfileRequestEvent.getAccessToken().getToken(), object.getString("email")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        EventBus.getDefault().post(new FacebookProfileRequestEvent(loginResult.getAccessToken()));
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException e) {

    }
}
