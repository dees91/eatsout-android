package pl.deesoft.eatsout.repository;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.orhanobut.logger.Logger;

import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.event.LocationUpdateRequestEvent;
import pl.deesoft.eatsout.event.LocationUpdatedEvent;

/**
 * Created by Piotr Krawczyk on 24.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PositionRepository {

    private boolean isGPSEnabled = true;

    private LatLng currentPosition;

    private LocationManager locationManager;

    private LocationListener locationWiFiListener;

    private LocationListener locationGPSListener;

    public PositionRepository(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationGPSListener = new MyLocationListener();
        locationWiFiListener = new MyLocationWiFiListener();

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 50, locationGPSListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 50, locationWiFiListener);
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    public void onEvent(LocationUpdateRequestEvent locationUpdateRequestEvent) {
        EventBus.getDefault().post(new LocationUpdatedEvent(getCurrentPosition()));
    }

    private LatLng getCurrentPosition() {
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);

        if (provider != null) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
            } else {
                location = getLastBestLocation();
                if (location != null) {
                    currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                }
            }
        }

        return currentPosition;
    }

    private Location getLastBestLocation() {
        long GPSLocationTime = 0, NetLocationTime = 0;
        try {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (locationGPS != null) {
                GPSLocationTime = locationGPS.getTime();
            }

            if (locationNet != null) {
                NetLocationTime = locationNet.getTime();
            }

            if (GPSLocationTime - NetLocationTime > 0) {
                return locationGPS;
            } else {
                return locationNet;
            }
        } catch (Exception e) {
            return null;
        }
    }

    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (locationWiFiListener != null && locationManager != null) {
                locationManager.removeUpdates(locationWiFiListener);
            }

            if (location != null) {
                currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                EventBus.getDefault().post(new LocationUpdatedEvent(currentPosition));
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        @Override
        public void onProviderDisabled(String provider) {
            isGPSEnabled = false;
        }

        @Override
        public void onProviderEnabled(String provider) {
            isGPSEnabled = true;
        }
    }

    private class MyLocationWiFiListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                EventBus.getDefault().post(new LocationUpdatedEvent(currentPosition));
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}
    }
}
