package pl.deesoft.eatsout.repository;

import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.api.Authentication;
import pl.deesoft.eatsout.api.callback.RestCallback;
import pl.deesoft.eatsout.api.model.RestError;
import pl.deesoft.eatsout.event.FacebookAuthenticatedEvent;
import pl.deesoft.eatsout.event.FacebookAuthenticationRequestEvent;
import pl.deesoft.eatsout.event.PasswordAuthenticatedEvent;
import pl.deesoft.eatsout.event.PasswordAuthenticationRequestEvent;
import pl.deesoft.eatsout.event.PasswordChangeErrorEvent;
import pl.deesoft.eatsout.event.PasswordChangeRequestEvent;
import pl.deesoft.eatsout.event.PasswordChangedEvent;
import pl.deesoft.eatsout.event.PasswordResetErrorEvent;
import pl.deesoft.eatsout.event.PasswordResetRequestEvent;
import pl.deesoft.eatsout.event.PasswordResettedEvent;
import pl.deesoft.eatsout.event.RegisterErrorEvent;
import pl.deesoft.eatsout.event.RegisterRequestEvent;
import pl.deesoft.eatsout.event.RegisteredEvent;
import retrofit.client.Response;

/**
 * Created by Piotr Krawczyk on 07.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class AuthenticationRepository {
    Authentication authentication;

    public AuthenticationRepository(Authentication authentication) {
        this.authentication = authentication;
    }

    public void onEvent(PasswordAuthenticationRequestEvent passwordAuthenticationRequestEvent) {
        authentication.passwordAuth(passwordAuthenticationRequestEvent, new RestCallback<PasswordAuthenticatedEvent>() {
            @Override
            public void success(PasswordAuthenticatedEvent passwordAuthenticatedEvent, Response response) {
                EventBus.getDefault().post(passwordAuthenticatedEvent);
            }

            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }
        });
    }

    public void onEvent(FacebookAuthenticationRequestEvent facebookAuthenticationRequestEvent) {
        authentication.facebookAuth(facebookAuthenticationRequestEvent, new RestCallback<FacebookAuthenticatedEvent>() {
            @Override
            public void success(FacebookAuthenticatedEvent facebookAuthenticatedEvent, Response response) {
                EventBus.getDefault().post(facebookAuthenticatedEvent);
            }

            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }
        });
    }

    public void onEvent(PasswordResetRequestEvent passwordResetRequestEvent) {
        authentication.resetPassword(passwordResetRequestEvent, new RestCallback<PasswordResettedEvent>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
                EventBus.getDefault().post(new PasswordResetErrorEvent());
            }

            @Override
            public void success(PasswordResettedEvent passwordResettedEvent, Response response) {
                EventBus.getDefault().post(passwordResettedEvent);
            }
        });
    }

    public void onEvent(PasswordChangeRequestEvent passwordChangeRequestEvent) {
        authentication.changePassword(passwordChangeRequestEvent, new RestCallback<PasswordChangedEvent>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
                EventBus.getDefault().post(new PasswordChangeErrorEvent());
            }

            @Override
            public void success(PasswordChangedEvent passwordChangedEvent, Response response) {
                EventBus.getDefault().post(passwordChangedEvent);
            }
        });
    }

    public void onEvent(RegisterRequestEvent registerRequestEvent) {
        authentication.register(registerRequestEvent.getUser(), new RestCallback<RegisteredEvent>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
                EventBus.getDefault().post(new RegisterErrorEvent());
            }

            @Override
            public void success(RegisteredEvent registeredEvent, Response response) {
                EventBus.getDefault().post(registeredEvent);
            }
        });
    }
}
