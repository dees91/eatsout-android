package pl.deesoft.eatsout.repository;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.orhanobut.logger.Logger;

import java.util.List;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import pl.deesoft.eatsout.api.ApiService;
import pl.deesoft.eatsout.api.callback.RestCallback;
import pl.deesoft.eatsout.api.model.RestError;
import pl.deesoft.eatsout.entity.Category;
import pl.deesoft.eatsout.entity.Follow;
import pl.deesoft.eatsout.entity.Opinion;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.entity.Tag;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.AccountUpdateRequestEvent;
import pl.deesoft.eatsout.event.AccountUpdatedEvent;
import pl.deesoft.eatsout.event.AvatarCreateRequestEvent;
import pl.deesoft.eatsout.event.AvatarCreatedEvent;
import pl.deesoft.eatsout.event.AvatarUpdateRequestEvent;
import pl.deesoft.eatsout.event.AvatarUpdatedEvent;
import pl.deesoft.eatsout.event.CategoryListLoadedEvent;
import pl.deesoft.eatsout.event.FollowCreateRequestEvent;
import pl.deesoft.eatsout.event.FollowCreatedEvent;
import pl.deesoft.eatsout.event.FollowDeleteRequestEvent;
import pl.deesoft.eatsout.event.FollowDeletedEvent;
import pl.deesoft.eatsout.event.LocationUpdatedEvent;
import pl.deesoft.eatsout.event.OpinionCreateRequestEvent;
import pl.deesoft.eatsout.event.OpinionCreatedEvent;
import pl.deesoft.eatsout.event.OpinionListLoadedEvent;
import pl.deesoft.eatsout.event.PhotoCreateRequestEvent;
import pl.deesoft.eatsout.event.PhotoCreatedEvent;
import pl.deesoft.eatsout.event.PhotoListLoadedEvent;
import pl.deesoft.eatsout.event.ResourceListRequestEvent;
import pl.deesoft.eatsout.event.RestaurantListLoadedEvent;
import pl.deesoft.eatsout.event.RestaurantListRequestEvent;
import pl.deesoft.eatsout.event.TagListLoadedEvent;
import pl.deesoft.eatsout.event.UserGetRequestEvent;
import pl.deesoft.eatsout.event.UserGettedEvent;
import pl.deesoft.eatsout.event.UserListLoadedEvent;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.DateFormat;
import retrofit.client.Response;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class ResourceRepository {
    ApiService apiService;
    ApiService paginatedApiService;
    LatLng currentPosition = new LatLng(52, 21);
    RestaurantListRequestEvent lastRestaurantListRequestEvent = null;

    public ResourceRepository(ApiService apiService, ApiService paginatedApiService) {
        this.apiService = apiService;
        this.paginatedApiService = paginatedApiService;
    }

    public void onEvent(PhotoCreateRequestEvent e) {
        apiService.createPhoto(e.getTitle(), e.getDescription(), e.getUser(), e.getTags(), e.getRestaurant(), e.getFile(), new RestCallback<Photo>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }

            @Override
            public void success(Photo photo, Response response) {
                EventBus.getDefault().post(new PhotoCreatedEvent(photo));
            }
        });
    }

    public void onEvent(final ResourceListRequestEvent e) {
        ApiService currentApiService = e.isPaginated() ? paginatedApiService : apiService;
        switch (e.getResource()) {
            case ResourceListRequestEvent.RESOURCE_TAG:
                apiService.getTags(false, 0, 0, null, null, new RestCallback<List<Tag>>() {
                    @Override
                    public void failure(RestError restError) {
                        EventBus.getDefault().post(restError);
                    }

                    @Override
                    public void success(List<Tag> tags, Response response) {
                        EventBus.getDefault().post(new TagListLoadedEvent(tags, e.getCacheKey()));
                    }
                });
                break;
            case ResourceListRequestEvent.RESOURCE_CATEGORY:
                apiService.getCategories(false, 0, 0, null, null, new RestCallback<List<Category>>() {
                    @Override
                    public void failure(RestError restError) {
                        EventBus.getDefault().post(restError);
                    }

                    @Override
                    public void success(List<Category> categories, Response response) {
                        EventBus.getDefault().post(new CategoryListLoadedEvent(categories, e.getCacheKey()));
                    }
                });
                break;
            case ResourceListRequestEvent.RESOURCE_USER:
                apiService.getUsers(false, 0, 0, null, null, new RestCallback<List<User>>() {
                    @Override
                    public void failure(RestError restError) {
                        EventBus.getDefault().post(restError);
                    }

                    @Override
                    public void success(List<User> users, Response response) {
                        EventBus.getDefault().post(new UserListLoadedEvent(users, e.getCacheKey()));
                    }
                });
                break;
            case ResourceListRequestEvent.RESOURCE_PHOTO:
                currentApiService.getPhotos(e.isPaginated(), e.getPage(), e.getLimit(), e.getCriteria().isEmpty() ? null : e.getCriteria(), null, new RestCallback<List<Photo>>() {
                    @Override
                    public void failure(RestError restError) {
                        EventBus.getDefault().post(restError);
                    }

                    @Override
                    public void success(List<Photo> photos, Response response) {
                        EventBus.getDefault().post(new PhotoListLoadedEvent(photos, e.getCacheKey()));
                    }
                });
                break;
            case ResourceListRequestEvent.RESOURCE_OPINION:
                currentApiService.getOpinions(e.isPaginated(), e.getPage(), e.getLimit(), e.getCriteria().isEmpty() ? null : e.getCriteria(), null, new RestCallback<List<Opinion>>() {
                    @Override
                    public void failure(RestError restError) {
                        EventBus.getDefault().post(restError);
                    }

                    @Override
                    public void success(List<Opinion> opinions, Response response) {
                        EventBus.getDefault().post(new OpinionListLoadedEvent(opinions, e.getCacheKey()));
                    }
                });
                break;
            default: break;
        }
    }

    @DebugLog
    public void onEvent(final RestaurantListRequestEvent e) {
        lastRestaurantListRequestEvent = e;
        apiService.getRestaurants(30, null, null, currentPosition.latitude, currentPosition.longitude, 10, new RestCallback<List<Restaurant>>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }

            @Override
            public void success(List<Restaurant> restaurants, Response response) {
                EventBus.getDefault().post(new RestaurantListLoadedEvent(restaurants, e.getCacheKey()));
            }
        });
    }

    @DebugLog
    public void onEvent(LocationUpdatedEvent e) {
        if (e.getLatLng() != null) {
            float[] results = new float[1];
            Location.distanceBetween(currentPosition.latitude, currentPosition.longitude, e.getLatLng().latitude, e.getLatLng().longitude, results);

            if (results[0] > Configuration.UPDATE_DISTANCE && lastRestaurantListRequestEvent != null) {
                EventBus.getDefault().post(lastRestaurantListRequestEvent);
            }

            currentPosition = e.getLatLng();
        }
    }

    public void onEvent(AccountUpdateRequestEvent e) {
        apiService.updateUser(e.getId(), e.getUsername(), e.getPlainPassword(), e.getEmail(), e.getFirstname(), e.getLastname(), e.getCity(), e.getCountry(), new RestCallback<User>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }

            @Override
            public void success(User user, Response response) {
                EventBus.getDefault().post(new AccountUpdatedEvent(user));
            }
        });
    }

    public void onEvent(AvatarCreateRequestEvent e) {
        apiService.createAvatar(e.getUser(), e.getFile(), new RestCallback<AvatarCreatedEvent>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }

            @Override
            public void success(AvatarCreatedEvent avatarCreatedEvent, Response response) {
                EventBus.getDefault().post(avatarCreatedEvent);
            }
        });
    }

    public void onEvent(AvatarUpdateRequestEvent e) {
        Logger.e(DateFormat.currentDate());
        apiService.updateAvatar(e.getId(), e.getUser(), DateFormat.currentDate(), e.getFile(), new RestCallback<AvatarUpdatedEvent>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }

            @Override
            public void success(AvatarUpdatedEvent avatarUpdatedEvent, Response response) {
                EventBus.getDefault().post(avatarUpdatedEvent);
            }
        });
    }

    public void onEvent(OpinionCreateRequestEvent e) {
        apiService.createOpinion(e.getRate(), e.getComment(), e.getRestaurant(), e.getUser(), new RestCallback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                EventBus.getDefault().post(new OpinionCreatedEvent());
            }

            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }
        });
    }

    public void onEvent(FollowCreateRequestEvent e) {
        apiService.followUser(e.getFollowing(), e.getFollowed(), new RestCallback<Follow>() {

            @Override
            public void success(Follow follow, Response response) {
                EventBus.getDefault().post(new FollowCreatedEvent(follow));
            }

            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }
        });
    }

    public void onEvent(final FollowDeleteRequestEvent e) {
        apiService.deleteFollow(e.getFollow().getId(), new RestCallback<Response>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }

            @Override
            public void success(Response response, Response response2) {
                EventBus.getDefault().post(new FollowDeletedEvent(e.getFollow()));
            }
        });
    }

    public void onEvent(final UserGetRequestEvent e) {
        apiService.getUser(e.getId(), new RestCallback<User>() {
            @Override
            public void failure(RestError restError) {
                EventBus.getDefault().post(restError);
            }

            @Override
            public void success(User user, Response response) {
                EventBus.getDefault().post(new UserGettedEvent(user, e.isCurrent()));
            }
        });
    }
}
