package pl.deesoft.eatsout.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;

import pl.deesoft.eatsout.util.Constants;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Piotr Krawczyk on 25.04.15.
 * krawczyk.piotr.91@gmail.com
 */
public class RestClient {
    private Context context;

    private Authentication authentication;
    private ApiService apiService;
    private ApiService paginatedApiService;

    public RestClient(Context context) {
        this.context = context;

        String dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm").toPattern();

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .setDateFormat(dateformat)
                .create();

        Gson paginatedGson = new GsonBuilder()
                .registerTypeAdapterFactory(new PaginatedItemTypeAdapterFactory())
                .setDateFormat(dateformat)
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Constants.API_BASE_URL)
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new SessionRequestInterceptor(context))
                .build();

        RestAdapter paginatedRestAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Constants.API_BASE_URL)
                .setConverter(new GsonConverter(paginatedGson))
                .setRequestInterceptor(new SessionRequestInterceptor(context))
                .build();

        authentication = restAdapter.create(Authentication.class);
        apiService = restAdapter.create(ApiService.class);
        paginatedApiService = paginatedRestAdapter.create(ApiService.class);
    }

    public Authentication getAuthentication() {
        return authentication;
    }

    public ApiService getApiService() {
        return apiService;
    }

    public ApiService getPaginatedApiService() {
        return paginatedApiService;
    }
}
