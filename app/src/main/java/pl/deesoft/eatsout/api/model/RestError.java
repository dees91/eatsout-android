package pl.deesoft.eatsout.api.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piotr Krawczyk on 26.04.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class RestError {

    @SerializedName("messages")
    private List<String> messages;

    public RestError() {
        messages = new ArrayList<>();
    }

    public RestError(List<String> messages)
    {
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
