package pl.deesoft.eatsout.api;

import pl.deesoft.eatsout.api.callback.RestCallback;
import pl.deesoft.eatsout.event.FacebookAuthenticatedEvent;
import pl.deesoft.eatsout.event.FacebookAuthenticationRequestEvent;
import pl.deesoft.eatsout.event.PasswordAuthenticatedEvent;
import pl.deesoft.eatsout.event.PasswordAuthenticationRequestEvent;
import pl.deesoft.eatsout.event.PasswordChangeRequestEvent;
import pl.deesoft.eatsout.event.PasswordChangedEvent;
import pl.deesoft.eatsout.event.PasswordResetRequestEvent;
import pl.deesoft.eatsout.event.PasswordResettedEvent;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.RegisteredEvent;
import pl.deesoft.eatsout.util.Constants;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Piotr Krawczyk on 25.04.15.
 * krawczyk.piotr.91@gmail.com
 */
public interface Authentication {

    @POST(Constants.API_AUTH_URL)
    public void passwordAuth(@Body PasswordAuthenticationRequestEvent passwordAuthenticationRequestEvent, RestCallback<PasswordAuthenticatedEvent> callback);

    @POST(Constants.API_AUTH_URL)
    public void facebookAuth(@Body FacebookAuthenticationRequestEvent facebookAuthenticationRequestEvent, RestCallback<FacebookAuthenticatedEvent> callback);

    @POST(Constants.API_RESET_PASSWORD)
    public void resetPassword(@Body PasswordResetRequestEvent passwordResetRequestEvent, RestCallback<PasswordResettedEvent> callback);

    @POST(Constants.API_CHANGE_PASSWORD)
    public void changePassword(@Body PasswordChangeRequestEvent passwordChangeRequestEvent, RestCallback<PasswordChangedEvent> callback);

    @POST(Constants.API_REGISTER)
    public void register(@Body User user, RestCallback<RegisteredEvent> callback);
}
