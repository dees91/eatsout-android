package pl.deesoft.eatsout.api;

import java.util.Date;
import java.util.HashMap;

import pl.deesoft.eatsout.event.PhotoListLoadedEvent;
import pl.deesoft.eatsout.event.ResourceListLoadedEvent;
import pl.deesoft.eatsout.util.Configuration;

/**
 * Created by Piotr Krawczyk on 11.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class QueryRegistry {

    HashMap<String, Long> queryRegistry;

    protected static QueryRegistry instance = null;

    protected QueryRegistry() {
        queryRegistry = new HashMap<>();
    }

    public static QueryRegistry getInstance() {
        if (instance == null) {
            instance = new QueryRegistry();
        }

        return instance;
    }

    public void onEvent(ResourceListLoadedEvent resourceListLoadedEvent) {
        queryRegistry.put(resourceListLoadedEvent.getCacheKey(), new Date().getTime());
    }

    public boolean shouldQuery(String key) {
        if (queryRegistry.containsKey(key)) {
            long prev = queryRegistry.get(key);
            long now = new Date().getTime();

            if (Math.abs(now - prev) <= Configuration.QUERY_DIFF) {
                return false;
            }
        }

        return true;
    }
}
