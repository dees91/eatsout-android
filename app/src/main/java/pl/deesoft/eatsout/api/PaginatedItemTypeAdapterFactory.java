package pl.deesoft.eatsout.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import hugo.weaving.DebugLog;

/**
 * Created by Piotr Krawczyk on 26.04.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PaginatedItemTypeAdapterFactory implements TypeAdapterFactory {

    @DebugLog
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<T>() {

            @DebugLog
            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            @DebugLog
            public T read(JsonReader in) throws IOException {

                JsonElement jsonElement = elementAdapter.read(in);

                if (jsonElement.isJsonObject()) {
                    JsonObject jsonObject = jsonElement.getAsJsonObject();
                    if (jsonObject.has("data") && (jsonObject.get("data").isJsonObject() || jsonObject.get("data").isJsonArray())) {
                        try {
                            JsonObject data = jsonObject.getAsJsonObject("data");
                            return delegate.fromJsonTree(data.get("items"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                return delegate.fromJsonTree(jsonElement);
            }
        }.nullSafe();
    }
}
