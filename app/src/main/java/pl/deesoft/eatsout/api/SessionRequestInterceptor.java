package pl.deesoft.eatsout.api;

import android.content.Context;

import com.orhanobut.hawk.Hawk;

import pl.deesoft.eatsout.util.Constants;
import retrofit.RequestInterceptor;

/**
 * Created by Piotr Krawczyk on 25.04.15.
 * krawczyk.piotr.91@gmail.com
 */
public class SessionRequestInterceptor implements RequestInterceptor {
    private Context context;

    public SessionRequestInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public void intercept(RequestFacade request) {
        String token = Hawk.get(Constants.SP_TOKEN);

        if (token != null) {
            request.addQueryParam(Constants.API_TOKEN_QUERY_NAME, token);
        }
    }
}
