package pl.deesoft.eatsout.api.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 02.05.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class Expiration {

    @SerializedName("date")
    String date;

    @SerializedName("timezone_type")
    int timezone_type;

    @SerializedName("timezone")
    String timezone;

    public Expiration() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTimezone_type() {
        return timezone_type;
    }

    public void setTimezone_type(int timezone_type) {
        this.timezone_type = timezone_type;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
