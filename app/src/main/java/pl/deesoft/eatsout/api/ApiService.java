package pl.deesoft.eatsout.api;

import java.util.HashMap;
import java.util.List;

import pl.deesoft.eatsout.api.callback.RestCallback;
import pl.deesoft.eatsout.entity.Category;
import pl.deesoft.eatsout.entity.Follow;
import pl.deesoft.eatsout.entity.Opinion;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.entity.Tag;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.AvatarCreatedEvent;
import pl.deesoft.eatsout.event.AvatarUpdatedEvent;
import pl.deesoft.eatsout.util.Constants;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

/**
 * Created by Piotr Krawczyk on 27.04.15.
 * krawczyk.piotr.91@gmail.com
 */
public interface ApiService {

    @GET(Constants.API_RESTAURANT_URL)
    void getRestaurants(
            @Query("limit") int limit,
            @Query("criteria") String criteria,
            @Query("sorting") String sorting,
            @Query("lat") double lat,
            @Query("lng") double lng,
            @Query("radius") float radius,
            RestCallback<List<Restaurant>> callback
    );

    @FormUrlEncoded
    @POST(Constants.API_RESTAURANT_URL)
    void createRestaurants(
            @Field("name") String name,
            Callback<Response> callback
    );

    @GET(Constants.API_PHOTOS_URL)
    void getPhotos(
            @Query("paginated") Boolean paginated,
            @Query("page") int page,
            @Query("limit") int limit,
            @QueryMap HashMap criteria,
            @Query("sorting") String sorting,
            RestCallback<List<Photo>> callback
    );

    @GET(Constants.API_TAG_URL)
    void getTags(
            @Query("paginated") Boolean paginated,
            @Query("page") int page,
            @Query("limit") int limit,
            @Query("criteria") String criteria,
            @Query("sorting") String sorting,
            RestCallback<List<Tag>> callback
    );

    @GET(Constants.API_CATEGORY_URL)
    void getCategories(
            @Query("paginated") Boolean paginated,
            @Query("page") int page,
            @Query("limit") int limit,
            @Query("criteria") String criteria,
            @Query("sorting") String sorting,
            RestCallback<List<Category>> callback
    );

    @GET(Constants.API_USER_URL)
    void getUsers(
            @Query("paginated") Boolean paginated,
            @Query("page") int page,
            @Query("limit") int limit,
            @Query("criteria") String criteria,
            @Query("sorting") String sorting,
            RestCallback<List<User>> callback
    );

    @FormUrlEncoded
    @PATCH(Constants.API_USER_URL + "{id}")
    void updateUser(
            @Path("id") long id,
            @Field("username") String username,
            @Field("plainPassword") String plainPassword,
            @Field("email") String email,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("city") String city,
            @Field("country") String country,
            RestCallback<User> callback
    );

    @Multipart
    @POST(Constants.API_PHOTOS_URL)
    void createPhoto(
            @Part("title") String title,
            @Part("description") String description,
            @Part("user") long user,
            @Part("tags") List<Tag> tags,
            @Part("restaurant") long restaurant,
            @Part("file") TypedFile file,
            RestCallback<Photo> callback
    );

    @Multipart
    @POST(Constants.API_AVATAR_URL)
    void createAvatar(
            @Part("user") long user,
            @Part("file") TypedFile file,
            RestCallback<AvatarCreatedEvent> callback
    );

    @Multipart
    @PATCH(Constants.API_AVATAR_URL + "{id}")
    void updateAvatar(
            @Path("id") long id,
            @Part("user") long user,
            @Part("modified_at") String modified_at,
            @Part("file") TypedFile file,
            RestCallback<AvatarUpdatedEvent> callback
    );

    @FormUrlEncoded
    @POST(Constants.API_OPINION_URL)
    void createOpinion(
            @Field("rate") float rate,
            @Field("comment") String comment,
            @Field("restaurant") long restaurant,
            @Field("user") long user,
            Callback<Response> callback
    );

    @FormUrlEncoded
    @POST(Constants.API_FOLLOW_URL)
    void followUser(
            @Field("following") long following,
            @Field("followed") long followed,
            Callback<Follow> callback
    );

    @DELETE(Constants.API_FOLLOW_URL + "{id}")
    void deleteFollow(
            @Path("id") long id,
            Callback<Response> callback
    );

    @GET(Constants.API_USER_URL + "{id}")
    void getUser(
            @Path("id") long id,
            Callback<User> callback
    );

    @GET(Constants.API_OPINION_URL)
    void getOpinions(
            @Query("paginated") Boolean paginated,
            @Query("page") int page,
            @Query("limit") int limit,
            @QueryMap HashMap criteria,
            @Query("sorting") String sorting,
            RestCallback<List<Opinion>> callback
    );
}
