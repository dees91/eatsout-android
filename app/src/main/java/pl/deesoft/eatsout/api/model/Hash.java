package pl.deesoft.eatsout.api.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 02.05.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class Hash {

    @SerializedName("expired_at")
    Expiration expired_at;

    public Hash() {
    }

    public Expiration getExpired_at() {
        return expired_at;
    }

    public void setExpired_at(Expiration expired_at) {
        this.expired_at = expired_at;
    }
}
