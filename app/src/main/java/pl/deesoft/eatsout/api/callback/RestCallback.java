package pl.deesoft.eatsout.api.callback;

import java.util.ArrayList;
import java.util.List;

import pl.deesoft.eatsout.api.model.RestError;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by Piotr Krawczyk on 26.04.15.
 * krawczyk.piotr.91@gmail.com
 */
public abstract class RestCallback<T> implements Callback<T> {

    public abstract void failure(RestError restError);

    @Override
    public void failure(final RetrofitError error) {
        RestError restError;
        if (error == null) {
            restError = new RestError();
        } else {
            restError = (RestError) error.getBodyAs(RestError.class);
            if (restError == null) {
                restError = new RestError(new ArrayList<String>(){{
                    add(error.getMessage());
                }});
            }
        }

        failure(restError);
    }
}
