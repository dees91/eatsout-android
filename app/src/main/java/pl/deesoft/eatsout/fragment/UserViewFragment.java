package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.orhanobut.hawk.Hawk;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.PhotoListAdapter;
import pl.deesoft.eatsout.adapter.UserOpinionListAdapter;
import pl.deesoft.eatsout.entity.Follow;
import pl.deesoft.eatsout.entity.Opinion;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.FollowCreateRequestEvent;
import pl.deesoft.eatsout.event.FollowCreatedEvent;
import pl.deesoft.eatsout.event.FollowDeleteRequestEvent;
import pl.deesoft.eatsout.event.FollowDeletedEvent;
import pl.deesoft.eatsout.event.OpinionListLoadedEvent;
import pl.deesoft.eatsout.event.PhotoListLoadedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.ResourceListRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.ui.ProgressCircular;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.util.UI;
import pl.deesoft.eatsout.util.UserProfile;

public class UserViewFragment extends BaseFragment {
    User user = null;
    Follow follow = null;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;
    PhotoListAdapter photoListAdapter;
    UserOpinionListAdapter opinionListAdapter;
    List<Photo> photoList;
    List<Opinion> opinionList;

    @InjectView(R.id.avatar) RoundedImageView avatar;
    @InjectView(R.id.inputsWrapper) LinearLayout inputsWrapper;
    @InjectView(R.id.follow) Button buttonFollow;
    @InjectView(R.id.unfollow) Button buttonUnfollow;
    @InjectView(R.id.photos) TextView photos;
    @InjectView(R.id.followed) TextView followed;
    @InjectView(R.id.following) TextView following;
    @InjectView(R.id.photosListView) ListView photosListView;
    @InjectView(R.id.photosWrapper) RelativeLayout photosWrapper;
    @InjectView(R.id.nope) TextView nope;
    @InjectView(R.id.photoProgress) ProgressCircular photoProgress;
    @InjectView(R.id.opinionListView) ListView opinionListView;
    @InjectView(R.id.opinionsWrapper) RelativeLayout opinionsWrapper;
    @InjectView(R.id.nopeOpinion) TextView nopeOpinion;
    @InjectView(R.id.opinionProgress) ProgressCircular opinionProgress;
    @InjectView(R.id.username) TextView username;
    @InjectView(R.id.city) TextView city;

    public UserViewFragment() {
        imageOptions = Configuration.getAvatarImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    public static UserViewFragment newInstance(User user) {
        UserViewFragment fragment = new UserViewFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.USER_SINGLE, Parcels.wrap(user));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = Parcels.unwrap(getArguments().getParcelable(Constants.USER_SINGLE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_user_view, container, false);
        ButterKnife.inject(this, fragmentView);

        photos.setText(user.getPhotos() + " photos");
        followed.setText(user.getFollowed() + " followers");
        following.setText((user.getFollowing() == null ? 0 : user.getFollowing().size()) + " following");

        imageLoader.displayImage(user.getAvatar() != null ? user.getAvatar().getFile() : null, avatar, imageOptions);

        username.setText(UserProfile.getUsername(user));
        if (UserProfile.hasCityOrCountry(user)) {
            city.setText(UserProfile.getCity(user));
        } else {
            city.setVisibility(View.GONE);
        }

        follow = currentUser.getFollowByUser(user);

        switchButtons(follow != null);

        photoList = Hawk.get(getCacheKey(), new ArrayList<Photo>());

        if (photoList.size() > 5) {
            photoList = photoList.subList(0, 5);
        }

        photoListAdapter = new PhotoListAdapter(mainActivity, R.layout.row_photo, photoList, photosListView);
        photosListView.setAdapter(photoListAdapter);
        photoListAdapter.notifyDataSetChanged();

        if (photoList.size() > 0) {
            photosListView.setVisibility(View.VISIBLE);
            photoProgress.setVisibility(View.GONE);
            UI.calculateListViewHeight(photosListView);
        } else {
            EventBus.getDefault().post(buildRequest());
        }

        opinionList = Hawk.get(getCacheKeyOpinion(), new ArrayList<Opinion>());

        if (opinionList.size() > 5) {
            opinionList = opinionList.subList(0, 5);
        }

        opinionListAdapter = new UserOpinionListAdapter(mainActivity, R.layout.row_opinion, opinionList, opinionListView);
        opinionListView.setAdapter(opinionListAdapter);
        opinionListAdapter.notifyDataSetChanged();

        if (opinionList.size() > 0) {
            opinionListView.setVisibility(View.VISIBLE);
            opinionProgress.setVisibility(View.GONE);
            UI.calculateListViewHeight(opinionListView);
        } else {
            EventBus.getDefault().post(opinionBuildRequest());
        }

        return fragmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (user != null) {
            outState.putParcelable(Constants.USER_SINGLE, Parcels.wrap(user));
        }
        super.onSaveInstanceState(outState);
    }

    @OnClick({R.id.userPhotos, R.id.photos}) void userPhotos() {
        EventBus.getDefault().post(new SwitchFragmentEvent(PhotoGridFragment.newInstance(user)));
    }

    @OnClick(R.id.follow) void follow() {
        EventBus.getDefault().post(new ProgressRequestEvent());
        setControlsEnabled(false, inputsWrapper);
        EventBus.getDefault().post(new FollowCreateRequestEvent(currentUser.getId(), user.getId()));
    }

    @OnClick(R.id.unfollow) void unfollow() {
        EventBus.getDefault().post(new ProgressRequestEvent());
        setControlsEnabled(false, inputsWrapper);
        EventBus.getDefault().post(new FollowDeleteRequestEvent(follow));
    }

    @OnClick(R.id.followed) void followed() {
//        EventBus.getDefault().post(new SwitchFragmentEvent(UsersFragment.newInstance(user.getFo)));
    }

    @OnClick(R.id.following) void following() {
//        EventBus.getDefault().post(new SwitchFragmentEvent(UsersFragment.newInstance(user.getFollowingUsers()), true));
    }

    public void onEvent(FollowCreatedEvent followCreatedEvent) {
        EventBus.getDefault().post(new ProgressEndEvent());
        setControlsEnabled(true, inputsWrapper);
        Crouton.makeText(mainActivity, "You are following " + user.getUsername() + " now.", Style.CONFIRM).show();
        switchButtons(true);
        follow = followCreatedEvent.getFollow();
        user.setFollowed(user.getFollowed() + 1);
    }

    public void onEvent(FollowDeletedEvent followDeletedEvent) {
        EventBus.getDefault().post(new ProgressEndEvent());
        setControlsEnabled(true, inputsWrapper);
        Crouton.makeText(mainActivity, "You are not longer following " + user.getUsername() + ".", Style.CONFIRM).show();
        switchButtons(false);
        follow = null;
        user.setFollowed(user.getFollowed() - 1);
    }

    public void onEvent(PhotoListLoadedEvent photoListLoadedEvent) {
        if (photoListAdapter != null) {
            photoProgress.setVisibility(View.GONE);
            if (photoListLoadedEvent.getPhotos().size() > 0) {
                photoListAdapter.replaceData(photoListLoadedEvent.getPhotos());
                photoListAdapter.notifyDataSetChanged();
                photosListView.setVisibility(View.VISIBLE);
                UI.calculateListViewHeight(photosListView);
            } else {
                nope.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onEvent(OpinionListLoadedEvent opinionListLoadedEvent) {
        if (opinionListAdapter != null) {
            opinionProgress.setVisibility(View.GONE);
            if (opinionListLoadedEvent.getOpinions().size() > 0) {
                opinionListAdapter.replaceData(opinionListLoadedEvent.getOpinions());
                opinionListAdapter.notifyDataSetChanged();
                opinionListView.setVisibility(View.VISIBLE);
                UI.calculateListViewHeight(opinionListView);
            } else {
                nopeOpinion.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected String getCacheKey() {
        return Constants.USER_PHOTOS_LIST + user.getId() + getClass().getName();
    }

    protected String getCacheKeyOpinion() {
        return Constants.USER_OPINIONS_LIST + user.getId() + getClass().getName();
    }

    private ResourceListRequestEvent buildRequest() {
        ResourceListRequestEvent resourceListRequestEvent = new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_PHOTO, getCacheKey());

        resourceListRequestEvent.setPaginated(true);
        resourceListRequestEvent.setPage(1);
        resourceListRequestEvent.setLimit(5);
        resourceListRequestEvent.addCriterium("user.id", String.valueOf(user.getId()));

        return resourceListRequestEvent;
    }

    private ResourceListRequestEvent opinionBuildRequest() {
        ResourceListRequestEvent resourceListRequestEvent = new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_OPINION, getCacheKeyOpinion());

        resourceListRequestEvent.setPaginated(true);
        resourceListRequestEvent.setPage(1);
        resourceListRequestEvent.setLimit(5);
        resourceListRequestEvent.addCriterium("user.id", String.valueOf(user.getId()));

        return resourceListRequestEvent;
    }

    private void switchButtons(boolean follow) {
        if (follow) {
            buttonFollow.setVisibility(View.GONE);
            buttonUnfollow.setVisibility(View.VISIBLE);
        } else {
            buttonFollow.setVisibility(View.VISIBLE);
            buttonUnfollow.setVisibility(View.GONE);
        }
    }
}
