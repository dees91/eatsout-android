package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.deesoft.eatsout.R;

public class MenuTitleFragment extends Fragment {

    public static final String TAG = "MENU_TITLE_FRAGMENT";

    public MenuTitleFragment() {}

    public static MenuTitleFragment newInstance() {
        return new MenuTitleFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_menu_title, container, false);
        return fragmentView;
    }
}
