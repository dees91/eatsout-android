package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.PhotoGridAdapter;
import pl.deesoft.eatsout.api.QueryRegistry;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.PhotoListLoadedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.ResourceListRequestEvent;
import pl.deesoft.eatsout.util.Constants;


public class PhotoGridFragment extends BaseFragment {

    List<Photo> photos;
    Restaurant restaurant;
    User user;
    PhotoGridAdapter photoGridAdapter;

    @InjectView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.gridview) GridView gridView;
    @InjectView(R.id.nope) TextView nope;

    public PhotoGridFragment() {}

    public static PhotoGridFragment newInstance(Restaurant restaurant) {
        PhotoGridFragment fragment = new PhotoGridFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        fragment.setArguments(args);

        return fragment;
    }

    public static PhotoGridFragment newInstance(User user) {
        PhotoGridFragment fragment = new PhotoGridFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.USER_SINGLE, Parcels.wrap(user));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            restaurant = Parcels.unwrap(getArguments().getParcelable(Constants.RESTAURANT_SINGLE));
            user = Parcels.unwrap(getArguments().getParcelable(Constants.USER_SINGLE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_photo_grid, container, false);
        ButterKnife.inject(this, fragmentView);

        if (QueryRegistry.getInstance().shouldQuery(getCacheKey())) {
            EventBus.getDefault().post(new ProgressRequestEvent());
            EventBus.getDefault().post(buildRequest());
        }

        photos = Hawk.get(getCacheKey(), new ArrayList<Photo>());

        photoGridAdapter = new PhotoGridAdapter(mainActivity, photos);
        gridView.setAdapter(photoGridAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(buildRequest());
            }
        });

        if (photos.size() <= 0) {
            nope.setVisibility(View.VISIBLE);
        } else {
            nope.setVisibility(View.GONE);
        }

        return fragmentView;
    }

    public void onEvent(PhotoListLoadedEvent photoListLoadedEvent) {
        if (photoGridAdapter != null) {
            photoGridAdapter.replaceData(photoListLoadedEvent.getPhotos());
            photoGridAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
        EventBus.getDefault().post(new ProgressEndEvent());
        if (photoListLoadedEvent.getPhotos().size() <= 0) {
            nope.setVisibility(View.VISIBLE);
        } else {
            nope.setVisibility(View.GONE);
        }
    }

    @Override
    protected String getCacheKey() {
        return (restaurant != null ? Constants.RESTAURANT_PHOTOS_LIST : Constants.USER_PHOTOS_LIST) + (restaurant != null ? restaurant.getId() : user.getId()) + getClass().getName();
    }

    private ResourceListRequestEvent buildRequest() {
        ResourceListRequestEvent resourceListRequestEvent = new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_PHOTO, getCacheKey());
        if (restaurant != null) {
            resourceListRequestEvent.addCriterium("restaurant.id", String.valueOf(restaurant.getId()));
        }
        if (user != null) {
            resourceListRequestEvent.addCriterium("user.id", String.valueOf(user.getId()));
        }

        return resourceListRequestEvent;
    }
}
