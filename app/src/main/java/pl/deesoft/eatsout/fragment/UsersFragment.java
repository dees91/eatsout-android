package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.orhanobut.hawk.Hawk;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.UserListAdapter;
import pl.deesoft.eatsout.api.QueryRegistry;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.ResourceListRequestEvent;
import pl.deesoft.eatsout.event.UserListLoadedEvent;
import pl.deesoft.eatsout.util.Constants;

public class UsersFragment extends BaseFragment {

    List<User> users;
    UserListAdapter userListAdapter;
    boolean givenList = false;

    @InjectView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.userListView) ListView userListView;

    public UsersFragment() {}

    public static UsersFragment newInstance() {
        return new UsersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            users = Parcels.unwrap(getArguments().getParcelable(Constants.USERS_LIST));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_users, container, false);
        ButterKnife.inject(this, fragmentView);

        if (QueryRegistry.getInstance().shouldQuery(getCacheKey())) {
            EventBus.getDefault().post(new ProgressRequestEvent());
            EventBus.getDefault().post(new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_USER, getCacheKey()));
        }

        users = Hawk.get(Constants.USERS_LIST, new ArrayList<User>());
        users.remove(currentUser);

        userListAdapter = new UserListAdapter(mainActivity, R.layout.row_user, users, userListView);

        userListView.setAdapter(userListAdapter);
        userListAdapter.notifyDataSetChanged();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_USER, getCacheKey()));
            }
        });

        return fragmentView;
    }

    public void onEvent(UserListLoadedEvent userListLoadedEvent) {
        if (userListAdapter != null) {
            userListLoadedEvent.getUsers().remove(currentUser);
            userListAdapter.replaceData(userListLoadedEvent.getUsers());
            userListAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    @Override
    protected String getCacheKey() {
        //todo
        return Constants.USERS_LIST + "_" + getClass().getName();
    }
}
