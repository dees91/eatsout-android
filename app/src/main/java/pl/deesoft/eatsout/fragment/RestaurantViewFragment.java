package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.parceler.Parcels;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.OpinionListAdapter;
import pl.deesoft.eatsout.entity.Category;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.ui.ProgressCircular;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.util.UI;


public class RestaurantViewFragment extends BaseFragment {
    Restaurant restaurant = null;
    List<Category> categories = null;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;
    GoogleMap googleMap = null;
    LatLng restaurantPosition = null;
    CameraPosition cameraPosition = null;
    OpinionListAdapter opinionListAdapter;

    @InjectView(R.id.categoryName) TextView categoryName;
    @InjectView(R.id.restaurantName) TextView restaurantName;
    @InjectView(R.id.address) TextView address;
    @InjectView(R.id.city) TextView city;
    @InjectView(R.id.cc) TextView cc;
    @InjectView(R.id.cover) ImageView cover;
    @InjectView(R.id.mapview) MapView mapView;
    @InjectView(R.id.rate) TextView rate;
    @InjectView(R.id.opinionList) ListView opinionList;
    @InjectView(R.id.scrollView) ScrollView scrollView;
    @InjectView(R.id.reviewsWrapper) RelativeLayout reviewsWrapper;
    @InjectView(R.id.reviewsAll) Button reviewsAll;
    @InjectView(R.id.opinionProgress) ProgressCircular opinionProgress;
    @InjectView(R.id.nope) TextView nope;

    public RestaurantViewFragment() {
        imageOptions = Configuration.getDefaultImageOptions();
        imageLoader = ImageLoader.getInstance();}

    public static RestaurantViewFragment newInstance(Restaurant restaurant) {
        RestaurantViewFragment fragment = new RestaurantViewFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            restaurant = Parcels.unwrap(getArguments().getParcelable(Constants.RESTAURANT_SINGLE));
            categories = restaurant != null ? restaurant.getCategories() : null;
            cameraPosition = Parcels.unwrap(getArguments().getParcelable(Constants.CAMERA_POSITION));
            restaurantPosition = new LatLng(restaurant != null ? restaurant.getLat() : 0, restaurant != null ? restaurant.getLng() : 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_restaurant_view, container, false);
        ButterKnife.inject(this, fragmentView);

        if (categories != null && categories.size() > 0) {
            categoryName.setText(categories.get(0).getName());
        }
        restaurantName.setText(restaurant.getName());
        address.setText(String.valueOf(restaurant.getAddress()).replace("null", ""));
        String citypostal = restaurant.getPostalCode() + " " + restaurant.getCity();
        city.setText(citypostal.replace("null", "").trim());
        cc.setText(String.valueOf(restaurant.getCountry()).replace("null", ""));
        rate.setText(restaurant.getRate() > 0 ? String.format("%.1f", restaurant.getRate()) : "-");
        imageLoader.displayImage(restaurant.getCover(), cover, imageOptions);

        opinionListAdapter = new OpinionListAdapter(mainActivity, R.layout.row_opinion, restaurant.getOpinions(), opinionList);
        opinionList.setAdapter(opinionListAdapter);
        opinionListAdapter.notifyDataSetChanged();

        if (restaurant.getOpinions() != null && restaurant.getOpinions().size() > 0) {
            UI.calculateListViewHeight(opinionList);
            opinionProgress.setVisibility(View.GONE);
            opinionList.setVisibility(View.VISIBLE);
        } else {
            reviewsAll.setVisibility(View.GONE);
            opinionProgress.setVisibility(View.GONE);
            nope.setVisibility(View.VISIBLE);
        }

        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @DebugLog
            @Override
            public void onMapReady(GoogleMap map) {
                map.setMyLocationEnabled(false);
                map.getUiSettings().setMapToolbarEnabled(false);
                map.addMarker(new MarkerOptions().position(restaurantPosition).title(restaurant.getName()));

                if (cameraPosition != null) {
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } else {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurantPosition, Configuration.MAP_ZOOM));
                }

                googleMap = map;
            }
        });

        return fragmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (googleMap != null) {
            outState.putParcelable(Constants.CAMERA_POSITION, googleMap.getCameraPosition());
        }
        if (restaurant != null) {
            outState.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @OnClick(R.id.buttonPhotos) void restaurantPhotos() {
        EventBus.getDefault().post(new SwitchFragmentEvent(PhotoGridFragment.newInstance(restaurant)));
    }

    @OnClick(R.id.buttonRate) void restaurantRate() {
        EventBus.getDefault().post(new SwitchFragmentEvent(AddOpinionFragment.newInstance(restaurant)));
    }

    @OnClick(R.id.mapWrapper) void goMap() {
        EventBus.getDefault().post(new SwitchFragmentEvent(RestaurantMapFragment.newInstance(restaurant)));
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }
}
