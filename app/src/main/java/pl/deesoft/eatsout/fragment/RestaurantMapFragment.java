package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import hugo.weaving.DebugLog;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;

public class RestaurantMapFragment extends BaseFragment {
    Restaurant restaurant = null;
    GoogleMap googleMap = null;
    LatLng restaurantPosition = null;
    CameraPosition cameraPosition = null;

    @InjectView(R.id.mapview) MapView mapView;

    public RestaurantMapFragment() {}

    public static RestaurantMapFragment newInstance(Restaurant restaurant) {
        RestaurantMapFragment fragment = new RestaurantMapFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            restaurant = Parcels.unwrap(getArguments().getParcelable(Constants.RESTAURANT_SINGLE));
            cameraPosition = Parcels.unwrap(getArguments().getParcelable(Constants.CAMERA_POSITION));
            restaurantPosition = new LatLng(restaurant != null ? restaurant.getLat() : 0, restaurant != null ? restaurant.getLng() : 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_restaurant_map, container, false);
        ButterKnife.inject(this, fragmentView);

        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @DebugLog
            @Override
            public void onMapReady(GoogleMap map) {
                map.setMyLocationEnabled(true);
                map.getUiSettings().setMapToolbarEnabled(false);
                map.addMarker(new MarkerOptions().position(restaurantPosition).title(restaurant.getName()));

                if (cameraPosition != null) {
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } else {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurantPosition, Configuration.MAP_ZOOM));
                }

                googleMap = map;
            }
        });

        return fragmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (googleMap != null) {
            outState.putParcelable(Constants.CAMERA_POSITION, googleMap.getCameraPosition());
        }
        if (restaurant != null) {
            outState.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @OnClick(R.id.backButton) void restaurantPhotos() {
        
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }
}
