package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.PhotoGridAdapter;
import pl.deesoft.eatsout.api.QueryRegistry;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.event.PhotoListLoadedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.ResourceListRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.util.Constants;

public class MainFragment extends BaseFragment {

    List<Photo> photos;
    PhotoGridAdapter photoGridAdapter;

    @InjectView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.gridview) GridView gridView;

    public MainFragment() {}

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @OnClick(R.id.buttonAddPhoto) void addPhoto() {
        EventBus.getDefault().post(new SwitchFragmentEvent(AddPhotoFragment.newInstance()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, fragmentView);

        if (QueryRegistry.getInstance().shouldQuery(getCacheKey())) {
            EventBus.getDefault().post(new ProgressRequestEvent());
            EventBus.getDefault().post(buildRequest(1));
        }

        if (Hawk.contains(getCacheKey())) {
            photos = Hawk.get(getCacheKey(), new ArrayList<Photo>());
        } else {
            photos = Hawk.get(Constants.PHOTOS_LIST, new ArrayList<Photo>());
        }

        photoGridAdapter = new PhotoGridAdapter(mainActivity, photos);
        gridView.setAdapter(photoGridAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(buildRequest(1));
            }
        });

        return fragmentView;
    }

    public void onEvent(PhotoListLoadedEvent photoListLoadedEvent) {
        if (photoGridAdapter != null) {
            photoGridAdapter.replaceData(photoListLoadedEvent.getPhotos());
            photoGridAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    @Override
    protected String getCacheKey() {
        return Constants.PHOTOS_LIST + "_" + getClass().getName();
    }

    private ResourceListRequestEvent buildRequest(int page) {
        ResourceListRequestEvent resourceListRequestEvent = new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_PHOTO, getCacheKey());

        resourceListRequestEvent.setPaginated(true);
        resourceListRequestEvent.setPage(1);
        resourceListRequestEvent.setLimit(10);

        return  resourceListRequestEvent;
    }
}
