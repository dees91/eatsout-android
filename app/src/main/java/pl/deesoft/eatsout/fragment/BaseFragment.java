package pl.deesoft.eatsout.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import hugo.weaving.DebugLog;
import pl.deesoft.eatsout.MainActivity;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.CurrentUserUpdatedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.ui.ProgressCircular;

/**
 * Created by Piotr Krawczyk on 05.05.15.
 * krawczyk.piotr.91@gmail.com
 */
abstract public class BaseFragment extends Fragment {

    protected MainActivity mainActivity;
    protected User currentUser;

    @InjectView(R.id.progress) ProgressCircular progress;

    public BaseFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mainActivity = (MainActivity) activity;
        } catch (ClassCastException e) {
            Logger.e(e.getMessage());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        currentUser = Hawk.get(Constants.CURRENT_USER);
    }

    @Override
    public void onDestroy() {
        Crouton.cancelAllCroutons();
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void onEvent(ProgressRequestEvent progressRequestEvent) {
        if (progress != null && progress.getVisibility() == View.GONE) {
            progress.post(new Runnable() {
                @Override
                public void run() {
                    progress.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public void onEvent(ProgressEndEvent progressEndEvent) {
        if (progress != null && progress.getVisibility() == View.VISIBLE) {
            progress.post(new Runnable() {
                @Override
                public void run() {
                    progress.setVisibility(View.GONE);
                }
            });
        }
    }

    @DebugLog
    public void onEvent(CurrentUserUpdatedEvent currentUserUpdatedEvent) {
        currentUser = currentUserUpdatedEvent.getUser();
    }

    protected void setControlsEnabled(boolean enable, ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++){
            View child = viewGroup.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                setControlsEnabled(enable, (ViewGroup)child);
            }
        }
    }

    protected void setControlsEnabled(boolean enable, LinearLayout linearLayout) {
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            View child = linearLayout.getChildAt(i);
            if (!(child instanceof ProgressCircular)) {
                child.setEnabled(enable);
            }
        }
    }

    abstract protected String getCacheKey();
}
