package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.CategoryListAdapter;
import pl.deesoft.eatsout.api.QueryRegistry;
import pl.deesoft.eatsout.entity.Category;
import pl.deesoft.eatsout.event.CategoryListLoadedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.ResourceListRequestEvent;
import pl.deesoft.eatsout.util.Constants;

public class CategoriesFragment extends BaseFragment {

    List<Category> categories;
    CategoryListAdapter categoryListAdapter;

    @InjectView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.categoryListView) ListView categoryListView;

    public CategoriesFragment() {}

    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_categories, container, false);
        ButterKnife.inject(this, fragmentView);

        if (QueryRegistry.getInstance().shouldQuery(getCacheKey())) {
            EventBus.getDefault().post(new ProgressRequestEvent());
            EventBus.getDefault().post(new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_CATEGORY, getCacheKey()));
        }

        categories = Hawk.get(Constants.CATEGORIES_LIST, new ArrayList<Category>());
        categoryListAdapter = new CategoryListAdapter(mainActivity, R.layout.row_category, categories, categoryListView);

        categoryListView.setAdapter(categoryListAdapter);
        categoryListAdapter.notifyDataSetChanged();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_CATEGORY, getCacheKey()));
            }
        });

        return fragmentView;
    }

    public void onEvent(CategoryListLoadedEvent categoryListLoadedEvent) {
        if (categoryListAdapter != null) {
            categoryListAdapter.replaceData(categoryListLoadedEvent.getCategories());
            categoryListAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    @Override
    protected String getCacheKey() {
        return Constants.CATEGORIES_LIST + "_" + getClass().getName();
    }
}
