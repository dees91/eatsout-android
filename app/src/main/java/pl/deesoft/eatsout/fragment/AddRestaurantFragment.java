package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import pl.deesoft.eatsout.R;

public class AddRestaurantFragment extends BaseFragment {

    public AddRestaurantFragment() {}

    public static AddRestaurantFragment newInstance() {
        return new AddRestaurantFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_add_restaurant, container, false);
        ButterKnife.inject(this, fragmentView);



        return fragmentView;
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }
}
