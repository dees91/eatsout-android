package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.orhanobut.hawk.Hawk;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.AccountUpdatedEvent;
import pl.deesoft.eatsout.event.AvatarCreatedEvent;
import pl.deesoft.eatsout.event.CurrentUserUpdatedEvent;
import pl.deesoft.eatsout.event.LogoutRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.util.UserProfile;


public class MenuFragment extends Fragment {

    public static final String TAG = "MENU_FRAGMENT";

    private User currentUser;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    @InjectView(R.id.userImage) RoundedImageView userImage;
    @InjectView(R.id.username) TextView username;

    @OnClick(R.id.buttonRestaurants) void restaurantsList() {
        EventBus.getDefault().post(new SwitchFragmentEvent(RestaurantsFragment.newInstance()));
    }

    @OnClick(R.id.userImage) void tagsList() {
        EventBus.getDefault().post(new SwitchFragmentEvent(AccountFragment.newInstance()));
    }

    @OnClick(R.id.buttonAddPhoto) void addPhoto() {
        EventBus.getDefault().post(new SwitchFragmentEvent(AddPhotoFragment.newInstance()));
    }

    @OnClick(R.id.buttonLogout) void logout() {
        EventBus.getDefault().post(new LogoutRequestEvent());
    }

    @OnClick(R.id.buttonAccount) void account() {
        EventBus.getDefault().post(new SwitchFragmentEvent(AccountFragment.newInstance()));
    }

    @OnClick(R.id.buttonUsers) void users() {
        EventBus.getDefault().post(new SwitchFragmentEvent(UsersFragment.newInstance()));
    }

    public MenuFragment() {
        imageOptions = Configuration.getAvatarImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        currentUser = Hawk.get(Constants.CURRENT_USER);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.inject(this, fragmentView);

        imageLoader.displayImage(currentUser.getAvatar() != null ? currentUser.getAvatar().toString() : null, userImage, imageOptions);
        username.setText(UserProfile.getUsername(currentUser));

        return fragmentView;
    }

    public void onEvent(AccountUpdatedEvent accountUpdatedEvent) {
        currentUser = accountUpdatedEvent.getUser();
        username.setText(UserProfile.getUsername(currentUser));
    }

    public void onEvent(AvatarCreatedEvent avatarCreatedEvent) {
        imageLoader.displayImage(avatarCreatedEvent.getFile(), userImage, imageOptions);
    }

    public void onEvent(CurrentUserUpdatedEvent currentUserUpdatedEvent) {
        currentUser = currentUserUpdatedEvent.getUser();
    }
}
