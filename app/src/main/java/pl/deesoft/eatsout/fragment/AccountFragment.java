package pl.deesoft.eatsout.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.AbstractUpdateEvent;
import pl.deesoft.eatsout.event.AccountUpdateRequestEvent;
import pl.deesoft.eatsout.event.AccountUpdatedEvent;
import pl.deesoft.eatsout.event.AvatarCreateRequestEvent;
import pl.deesoft.eatsout.event.AvatarCreatedEvent;
import pl.deesoft.eatsout.event.AvatarSelectedEvent;
import pl.deesoft.eatsout.event.AvatarUpdateRequestEvent;
import pl.deesoft.eatsout.event.AvatarUpdatedEvent;
import pl.deesoft.eatsout.event.PhotoCameraRequestEvent;
import pl.deesoft.eatsout.event.PhotoGalleryRequestEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import retrofit.mime.TypedFile;

public class AccountFragment extends BaseFragment implements AbstractUpdateEvent.UpdateEventBuilder {

    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;
    String pictureUri = null;

    @InjectView(R.id.usernameInput) EditText usernameInput;
    @InjectView(R.id.passwordInput) EditText passwordInput;
    @InjectView(R.id.emailInput) EditText emailInput;
    @InjectView(R.id.firstnameInput) EditText firstnameInput;
    @InjectView(R.id.lastnameInput) EditText lastnameInput;
    @InjectView(R.id.inputsWrapper) LinearLayout inputsWrapper;
    @InjectView(R.id.avatar) ImageView avatar;
    @InjectView(R.id.newAvatar) ImageView newAvatar;

    public AccountFragment() {
        imageOptions = Configuration.getAvatarImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    public static AccountFragment newInstance() {
        return new AccountFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.inject(this, fragmentView);

        userToInputs(currentUser);
        imageLoader.displayImage(currentUser.getAvatar() != null ? currentUser.getAvatar().getFile() : null, avatar, imageOptions);

        return fragmentView;
    }

    public void onEvent(AccountUpdatedEvent accountUpdatedEvent) {
        userToInputs(accountUpdatedEvent.getUser());
        setControlsEnabled(true, inputsWrapper);
    }

    public void onEvent(AvatarSelectedEvent avatarSelectedEvent) {
        pictureUri = avatarSelectedEvent.getUri().toString();
        imageLoader.displayImage(pictureUri, newAvatar, imageOptions);
    }

    public void onEvent(AvatarCreatedEvent avatarUpdatedEvent) {
        pictureUri = null;
        currentUser = avatarUpdatedEvent.getUser();
        newAvatar.setImageResource(android.R.color.transparent);
        imageLoader.displayImage(avatarUpdatedEvent.getFile(), avatar, imageOptions);
        setControlsEnabled(true, inputsWrapper);
        Crouton.makeText(mainActivity, "Avatar successfully updated.", Style.CONFIRM).show();
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    @OnClick(R.id.update) void update() {
        new AlertDialog.Builder(mainActivity)
                .setTitle("Are you sure?")
                .setMessage("Are you really want to update?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AccountUpdateRequestEvent accountUpdateRequestEvent = buildRequestEvent();
                        if (accountUpdateRequestEvent != null) {
                            setControlsEnabled(false, inputsWrapper);
                            EventBus.getDefault().post(accountUpdateRequestEvent);
                            EventBus.getDefault().post(new ProgressRequestEvent());
                        }
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @OnClick(R.id.avatarUpload) void avatarUpload() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setTitle("Upload avatar from...");
        builder.setItems(new CharSequence[]{"Camera", "Gallery", "Cancel"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        EventBus.getDefault().post(new PhotoCameraRequestEvent(Constants.REQUEST_CODE_AVATAR_FROM_CAMERA));
                        break;
                    case 1:
                        EventBus.getDefault().post(new PhotoGalleryRequestEvent(Constants.REQUEST_CODE_AVATAR_FROM_GALLERY));
                        break;
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    @OnClick(R.id.avatarChange) void avatarChange() {
        if (pictureUri != null && !pictureUri.equals("")) {
            if (currentUser.getAvatar() != null) {
                AvatarUpdateRequestEvent avatarUpdateRequestEvent = new AvatarUpdateRequestEvent(
                        currentUser.getAvatar().getId(),
                        currentUser.getId(),
                        new TypedFile("image/jpg", imageLoader.getDiskCache().get(pictureUri))
                );
                EventBus.getDefault().post(avatarUpdateRequestEvent);
            } else {
                AvatarCreateRequestEvent avatarCreateRequestEvent = new AvatarCreateRequestEvent(
                        currentUser.getId(),
                        new TypedFile("image/jpg", imageLoader.getDiskCache().get(pictureUri))
                );
                EventBus.getDefault().post(avatarCreateRequestEvent);
            }
            setControlsEnabled(false, inputsWrapper);
            EventBus.getDefault().post(new ProgressRequestEvent());
        } else {
            Crouton.makeText(mainActivity, "Please select new avatar.", Style.ALERT).show();
        }
    }

    @OnClick(R.id.myPhotos) void myPhotos() {
        EventBus.getDefault().post(new SwitchFragmentEvent(PhotoGridFragment.newInstance(currentUser)));
    }

    @Override
    public AccountUpdateRequestEvent buildRequestEvent() {
        AccountUpdateRequestEvent accountUpdateRequestEvent = new AccountUpdateRequestEvent(currentUser.getId());
        accountUpdateRequestEvent.setUsername(usernameInput.getText().toString().trim());
        accountUpdateRequestEvent.setEmail(emailInput.getText().toString().trim());
        accountUpdateRequestEvent.setPlainPassword(passwordInput.getText().toString().trim());
        accountUpdateRequestEvent.setFirstname(firstnameInput.getText().toString().trim());
        accountUpdateRequestEvent.setLastname(lastnameInput.getText().toString().trim());

        if (accountUpdateRequestEvent.getUsername().length() < 3) {
            Crouton.makeText(mainActivity, "Username should contains at least 3 characters.", Style.ALERT).show();
            return null;
        }

        if (accountUpdateRequestEvent.getEmail().length() < 3) {
            Crouton.makeText(mainActivity, "Email should be valid.", Style.ALERT).show();
            return null;
        }

        if (accountUpdateRequestEvent.getPlainPassword().length() < 3) {
            Crouton.makeText(mainActivity, "Password should contains at least 3 characters.", Style.ALERT).show();
            return null;
        }

        return accountUpdateRequestEvent;
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }

    private void userToInputs(User user) {
        usernameInput.setText(user.getUsername());
        emailInput.setText(user.getEmail());
        passwordInput.setText("");
        firstnameInput.setText(user.getFirstname());
        lastnameInput.setText(user.getLastname());
    }
}
