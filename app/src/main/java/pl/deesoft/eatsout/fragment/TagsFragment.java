package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.TagListAdapter;
import pl.deesoft.eatsout.api.QueryRegistry;
import pl.deesoft.eatsout.entity.Tag;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.ResourceListRequestEvent;
import pl.deesoft.eatsout.event.TagListLoadedEvent;
import pl.deesoft.eatsout.util.Constants;

public class TagsFragment extends BaseFragment {

    List<Tag> tags;
    TagListAdapter tagListAdapter;

    @InjectView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.tagListView) ListView tagListView;

    public TagsFragment() {}

    public static TagsFragment newInstance() {
        return new TagsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_tags, container, false);
        ButterKnife.inject(this, fragmentView);

        if (QueryRegistry.getInstance().shouldQuery(getCacheKey())) {
            EventBus.getDefault().post(new ProgressRequestEvent());
            EventBus.getDefault().post(new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_TAG, getCacheKey()));
        }

        tags = Hawk.get(Constants.TAGS_LIST, new ArrayList<Tag>());
        tagListAdapter = new TagListAdapter(mainActivity, R.layout.row_tag, tags, tagListView);

        tagListView.setAdapter(tagListAdapter);
        tagListAdapter.notifyDataSetChanged();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new ResourceListRequestEvent(ResourceListRequestEvent.RESOURCE_TAG, getCacheKey()));
            }
        });

        return fragmentView;
    }

    public void onEvent(TagListLoadedEvent tagListLoadedEvent) {
        if (tagListAdapter != null) {
            tagListAdapter.replaceData(tagListLoadedEvent.getTags());
            tagListAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    @Override
    protected String getCacheKey() {
        return Constants.TAGS_LIST + "_" + getClass().getName();
    }
}
