package pl.deesoft.eatsout.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.RestaurantListAdapter;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.event.RestaurantListLoadedEvent;
import pl.deesoft.eatsout.event.RestaurantListRequestEvent;
import pl.deesoft.eatsout.event.RestaurantSelectedEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.util.Constants;


public class RestaurantSelectFragment extends BaseFragment {

    List<Restaurant> restaurants;
    RestaurantSelectListAdapter restaurantListAdapter;

    @InjectView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.restaurantListView) ListView restaurantListView;

    public RestaurantSelectFragment() {}

    public static RestaurantSelectFragment newInstance() {
        return new RestaurantSelectFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_restaurant_select, container, false);
        ButterKnife.inject(this, fragmentView);

        restaurants = Hawk.get(Constants.RESTAURANTS_LIST, new ArrayList<Restaurant>());
        restaurantListAdapter = new RestaurantSelectListAdapter(mainActivity, R.layout.row_restaurant, restaurants, restaurantListView);

        restaurantListView.setAdapter(restaurantListAdapter);
        restaurantListAdapter.notifyDataSetChanged();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new RestaurantListRequestEvent(getCacheKey()));
            }
        });

        return fragmentView;
    }

    public void onEvent(RestaurantListLoadedEvent restaurantListLoadedEvent) {
        if (restaurantListAdapter != null) {
            restaurantListAdapter.replaceData(restaurantListLoadedEvent.getRestaurants());
            restaurantListAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.buttonAddRestaurant) void addRestaurant() {
        EventBus.getDefault().post(new SwitchFragmentEvent(AddRestaurantFragment.newInstance()));
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }

    public class RestaurantSelectListAdapter extends RestaurantListAdapter {
        public RestaurantSelectListAdapter(Context context, int resource, List<Restaurant> restaurants, ListView listView) {
            super(context, resource, restaurants, listView);
        }

        @Override
        protected View.OnClickListener setOnClickListener(final Restaurant restaurant) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new RestaurantSelectedEvent(restaurant));
                    EventBus.getDefault().post(new SwitchFragmentEvent(AddPhotoFragment.newInstance()));
                }
            };
        }
    }
}
