package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.parceler.Parcels;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.util.DateFormat;
import pl.deesoft.eatsout.util.UserProfile;

public class PhotoViewFragment extends BaseFragment {

    Photo photo;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    @InjectView(R.id.picture) ImageView imageView;
    @InjectView(R.id.restaurant) TextView restaurant;
    @InjectView(R.id.username) TextView username;
    @InjectView(R.id.date) TextView date;
    @InjectView(R.id.title) TextView title;
    @InjectView(R.id.comment) TextView description;

    public PhotoViewFragment() {
        imageOptions = Configuration.getDefaultImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    public static PhotoViewFragment newInstance(Photo photo) {
        PhotoViewFragment fragment = new PhotoViewFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.PHOTO_SINGLE, Parcels.wrap(photo));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            photo = Parcels.unwrap(getArguments().getParcelable(Constants.PHOTO_SINGLE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_photo_view, container, false);
        ButterKnife.inject(this, fragmentView);

        imageLoader.displayImage(Constants.WEBROOT_URL + photo.getFile(), imageView, imageOptions);
        restaurant.setText(photo.getRestaurant().getName());
        username.setText(UserProfile.getUsername(photo.getUser()));
        date.setText(DateFormat.format(photo.getDate()));
        if (photo.getTitle() != null && !photo.getTitle().equals("")) {
            title.setText(photo.getTitle());
        } else {
            title.setVisibility(View.GONE);
        }

        if (photo.getDescription() != null && !photo.getDescription().equals("")) {
            description.setText(photo.getDescription());
        } else {
            description.setVisibility(View.GONE);
        }

        return fragmentView;
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }

    @OnClick(R.id.restaurant) void goRestaurant() {
        EventBus.getDefault().post(new SwitchFragmentEvent(RestaurantViewFragment.newInstance(photo.getRestaurant())));
    }

    @OnClick(R.id.username) void goUser() {
        EventBus.getDefault().post(new SwitchFragmentEvent(UserViewFragment.newInstance(photo.getUser())));
    }
}
