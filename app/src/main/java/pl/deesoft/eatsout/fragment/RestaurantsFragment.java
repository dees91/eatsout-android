package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.RestaurantListAdapter;
import pl.deesoft.eatsout.api.QueryRegistry;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.RestaurantListLoadedEvent;
import pl.deesoft.eatsout.event.RestaurantListRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.util.Constants;

public class RestaurantsFragment extends BaseFragment {

    List<Restaurant> restaurants;
    RestaurantListAdapter restaurantListAdapter;

    @InjectView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.restaurantListView) ListView restaurantListView;

    public RestaurantsFragment() {}

    public static RestaurantsFragment newInstance() {
        return new RestaurantsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_restaurants, container, false);
        ButterKnife.inject(this, fragmentView);

        if (QueryRegistry.getInstance().shouldQuery(getCacheKey())) {
            EventBus.getDefault().post(new ProgressRequestEvent());
            EventBus.getDefault().post(new RestaurantListRequestEvent(getCacheKey()));
        }

        restaurants = Hawk.get(Constants.RESTAURANTS_LIST, new ArrayList<Restaurant>());
        restaurantListAdapter = new RestaurantListAdapter(mainActivity, R.layout.row_restaurant, restaurants, restaurantListView);

        restaurantListView.setAdapter(restaurantListAdapter);
        restaurantListAdapter.notifyDataSetChanged();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new RestaurantListRequestEvent(getCacheKey()));
            }
        });

        return fragmentView;
    }

    public void onEvent(RestaurantListLoadedEvent restaurantListLoadedEvent) {
        if (restaurantListAdapter != null) {
            restaurantListAdapter.replaceData(restaurantListLoadedEvent.getRestaurants());
            restaurantListAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    @OnClick(R.id.buttonAddRestaurant) void addRestaurant() {
        EventBus.getDefault().post(new SwitchFragmentEvent(AddRestaurantFragment.newInstance()));
    }

    @Override
    protected String getCacheKey() {
        return Constants.RESTAURANTS_LIST + "_" + getClass().getName();
    }
}
