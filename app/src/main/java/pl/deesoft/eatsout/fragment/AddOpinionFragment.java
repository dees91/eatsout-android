package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import org.parceler.Parcels;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.event.OpinionCreateRequestEvent;
import pl.deesoft.eatsout.event.OpinionCreatedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.util.Constants;

public class AddOpinionFragment extends BaseFragment {

    Restaurant restaurant;
    @InjectView(R.id.comment) EditText comment;
    @InjectView(R.id.ratingBar) RatingBar ratingBar;
    @InjectView(R.id.inputsWrapper) LinearLayout inputsWrapper;

    public AddOpinionFragment() {
    }

    public static AddOpinionFragment newInstance(Restaurant restaurant) {
        AddOpinionFragment fragment = new AddOpinionFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            restaurant = Parcels.unwrap(getArguments().getParcelable(Constants.RESTAURANT_SINGLE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_add_opinion, container, false);
        ButterKnife.inject(this, fragmentView);

        return fragmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (restaurant != null) {
            outState.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        }
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.buttonDetails) void restaurantDetails() {
        EventBus.getDefault().post(new SwitchFragmentEvent(RestaurantViewFragment.newInstance(restaurant)));
    }

    @OnClick(R.id.addReview) void addReview() {
        if (ratingBar.getRating() <= 0) {
            Crouton.makeText(mainActivity, "Please select rate.", Style.ALERT).show();
            return;
        }
        if (comment.getText().toString().trim().length() < 3) {
            Crouton.makeText(mainActivity, "Please enter your comment.", Style.ALERT).show();
            return;
        }
        OpinionCreateRequestEvent opinionCreateRequestEvent = new OpinionCreateRequestEvent();
        opinionCreateRequestEvent.setUser(currentUser.getId());
        opinionCreateRequestEvent.setRestaurant(restaurant.getId());
        opinionCreateRequestEvent.setComment(comment.getText().toString().trim());
        opinionCreateRequestEvent.setRate(ratingBar.getRating());

        setControlsEnabled(false, inputsWrapper);
        EventBus.getDefault().post(new ProgressRequestEvent());
        EventBus.getDefault().post(opinionCreateRequestEvent);
    }

    public void onEvent(OpinionCreatedEvent opinionCreatedEvent) {
        setControlsEnabled(true, inputsWrapper);
        EventBus.getDefault().post(new ProgressEndEvent());
        ratingBar.setRating(0.0f);
        comment.setText("");
    }

    @OnClick(R.id.buttonPhotos) void restaurantPhotos() {
        EventBus.getDefault().post(new SwitchFragmentEvent(PhotoGridFragment.newInstance(restaurant)));
    }

    @Override
    protected String getCacheKey() {
        return null;
    }
}

