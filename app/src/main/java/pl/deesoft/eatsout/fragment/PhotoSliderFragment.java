package pl.deesoft.eatsout.fragment;


import android.os.Bundle;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.PhotoSliderAdapter;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.util.Constants;

public class PhotoSliderFragment extends BaseFragment {

    PhotoSliderAdapter photoSliderAdapter;
    List<Photo> photos;
    int position;

    @InjectView(R.id.viewPager) ViewPager viewPager;
    @InjectView(R.id.pagerHeader) PagerTabStrip pagerTabStrip;

    public PhotoSliderFragment() {}

    public static PhotoSliderFragment newInstance(List<Photo> photos, int position) {
        PhotoSliderFragment fragment = new PhotoSliderFragment();
        Bundle args = new Bundle();

        args.putParcelable(Constants.PHOTOS_LIST, Parcels.wrap(photos));
        args.putInt(Constants.PHOTO_POSITION, position);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            photos = Parcels.unwrap(getArguments().getParcelable(Constants.PHOTOS_LIST));
            position = getArguments().getInt(Constants.PHOTO_POSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_photo_slider, container, false);
        ButterKnife.inject(this, fragmentView);

        photoSliderAdapter = new PhotoSliderAdapter(mainActivity.getSupportFragmentManager(), photos);
        viewPager.setAdapter(photoSliderAdapter);
        viewPager.setCurrentItem(position);

        return fragmentView;
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }
}
