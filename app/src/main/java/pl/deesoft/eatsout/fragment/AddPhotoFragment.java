package pl.deesoft.eatsout.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.orhanobut.hawk.Hawk;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.adapter.TagAutocompleteAdapter;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.entity.Tag;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.AbstractUpdateEvent;
import pl.deesoft.eatsout.event.PhotoCameraRequestEvent;
import pl.deesoft.eatsout.event.PhotoCreateRequestEvent;
import pl.deesoft.eatsout.event.PhotoCreatedEvent;
import pl.deesoft.eatsout.event.PhotoGalleryRequestEvent;
import pl.deesoft.eatsout.event.PhotoSelectedEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.RestaurantSelectRequestEvent;
import pl.deesoft.eatsout.event.RestaurantSelectedEvent;
import pl.deesoft.eatsout.ui.TagCompletionView;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import retrofit.mime.TypedFile;

public class AddPhotoFragment extends BaseFragment implements AbstractUpdateEvent.UpdateEventBuilder {

    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    List<Tag> tags;
    ArrayAdapter<Tag> adapter;
    String pictureUri = null;
    String title = null;
    String description = null;
    Restaurant restaurant = null;
    List<Tag> selectedTags = null;
    TypedFile typedFile = null;

    @InjectView(R.id.picture) ImageView imageView;
    @InjectView(R.id.tagSelect) TagCompletionView tagCompletionView;
    @InjectView(R.id.restaurantName) TextView restaurantName;
    @InjectView(R.id.inputDescription) EditText inputDescription;
    @InjectView(R.id.inputTitle) EditText inputTitle;
    @InjectView(R.id.scrollView) ScrollView scrollView;
    @InjectView(R.id.inputsWrapper) LinearLayout inputsWrapper;

    public AddPhotoFragment() {
        imageOptions = Configuration.getDefaultImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    public static AddPhotoFragment newInstance() {
        return new AddPhotoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_add_photo, container, false);
        ButterKnife.inject(this, fragmentView);

        tags = Hawk.get(Constants.TAGS_LIST, new ArrayList<Tag>());

        adapter = new TagAutocompleteAdapter(mainActivity, R.layout.entry_tag, tags);

        tagCompletionView.setAdapter(adapter);

        if (pictureUri != null) {
            imageLoader.displayImage(pictureUri, imageView, imageOptions);
        }

        if (restaurant != null) {
            restaurantName.setText(restaurant.getName());
        }
        return fragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            pictureUri = savedInstanceState.getString(Constants.PICTURE_URI);
            restaurant = Parcels.unwrap(savedInstanceState.getParcelable(Constants.RESTAURANT_SINGLE));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (pictureUri != null) {
            outState.putString(Constants.PICTURE_URI, pictureUri);
        }
        if (restaurant != null) {
            outState.putParcelable(Constants.RESTAURANT_SINGLE, Parcels.wrap(restaurant));
        }
        super.onSaveInstanceState(outState);
    }

    public void onEvent(PhotoSelectedEvent photoSelectedEvent) {
        pictureUri = photoSelectedEvent.getUri().toString();
        imageLoader.displayImage(pictureUri, imageView, imageOptions);
    }

    public void onEvent(RestaurantSelectedEvent restaurantSelectedEvent) {
        restaurant = restaurantSelectedEvent.getRestaurant();
        restaurantName.setText(restaurant.getName());
    }

    public void onEvent(PhotoCreatedEvent photoCreatedEvent) {
        clearInputs();
        setControlsEnabled(true, inputsWrapper);
    }

    @OnClick(R.id.buttonSelectPhoto) void selectPhoto() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setTitle("Add photo from...");
        builder.setItems(new CharSequence[]{"Camera", "Gallery", "Cancel"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        EventBus.getDefault().post(new PhotoCameraRequestEvent(Constants.REQUEST_CODE_IMAGE_FROM_CAMERA));
                        break;
                    case 1:
                        EventBus.getDefault().post(new PhotoGalleryRequestEvent(Constants.REQUEST_CODE_IMAGE_FROM_GALLERY));
                        break;
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    @OnClick(R.id.buttonSelectRestaurant) void selectRestaurant() {
        EventBus.getDefault().post(new RestaurantSelectRequestEvent());
    }

    @OnClick(R.id.buttonAddPhoto) void addPhoto() {
        PhotoCreateRequestEvent photoCreateRequestEvent = buildRequestEvent();
        if (photoCreateRequestEvent != null) {
            setControlsEnabled(false, inputsWrapper);
            EventBus.getDefault().post(photoCreateRequestEvent);
            EventBus.getDefault().post(new ProgressRequestEvent());
        }
    }

    @Override
    public PhotoCreateRequestEvent buildRequestEvent() {
        title = inputTitle.getText().toString().trim();
        if (title.length() < 3) {
            Crouton.makeText(mainActivity, "Title should contains at least 3 characters.", Style.ALERT).show();
            return null;
        }

        description = inputDescription.getText().toString().trim();
        if (description.length() < 3) {
            Crouton.makeText(mainActivity, "Description should contains at least 3 characters.", Style.ALERT).show();
            return null;
        }

        if (pictureUri != null) {
            typedFile = new TypedFile("image/jpg", imageLoader.getDiskCache().get(pictureUri));
        } else {
            Crouton.makeText(mainActivity, "You should select photo.", Style.ALERT).show();
            return null;
        }

        if (restaurant == null) {
            Crouton.makeText(mainActivity, "You should select restaurant.", Style.ALERT).show();
            return null;
        }

        List<Object> objects = tagCompletionView.getObjects();
        selectedTags = new ArrayList<>();

        for (Object object : objects) {
            Tag tag = (Tag) object;
            if (tag != null) {
                selectedTags.add(tag);
            }
        }

        //todo check this
//        if (selectedTags.size() <= 0) {
//            Crouton.makeText(mainActivity, "Please add at least one tag.", Style.ALERT).show();
//            return null;
//        }

        PhotoCreateRequestEvent photoCreateRequestEvent = new PhotoCreateRequestEvent();
        photoCreateRequestEvent.setTitle(title);
        photoCreateRequestEvent.setDescription(description);
        photoCreateRequestEvent.setFile(typedFile);
        photoCreateRequestEvent.setUser(((User) Hawk.get(Constants.CURRENT_USER)).getId());
        photoCreateRequestEvent.setRestaurant(restaurant.getId());
        photoCreateRequestEvent.setTags(selectedTags);

        return photoCreateRequestEvent;
    }

    @Override
    protected String getCacheKey() {
        return getClass().getName();
    }

    private void clearInputs() {
        title = null;
        description = null;
        typedFile = null;
        restaurant = null;
        selectedTags = null;

        imageView.setImageResource(android.R.color.transparent);
        tagCompletionView.clear();
        restaurantName.setText("");
        inputDescription.setText("");
        inputTitle.setText("");
    }
}
