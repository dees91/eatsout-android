package pl.deesoft.eatsout;

import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.orhanobut.hawk.Hawk;

import java.util.Date;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import pl.deesoft.eatsout.api.QueryRegistry;
import pl.deesoft.eatsout.api.RestClient;
import pl.deesoft.eatsout.entity.Avatar;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.AccountUpdateRequestEvent;
import pl.deesoft.eatsout.event.ActivityFinishRequestEvent;
import pl.deesoft.eatsout.event.ActivityRedirectEvent;
import pl.deesoft.eatsout.event.ApplicationReadyEvent;
import pl.deesoft.eatsout.event.AvatarCreateRequestEvent;
import pl.deesoft.eatsout.event.AvatarCreatedEvent;
import pl.deesoft.eatsout.event.AvatarUpdatedEvent;
import pl.deesoft.eatsout.event.CategoryListLoadedEvent;
import pl.deesoft.eatsout.event.CurrentUserUpdatedEvent;
import pl.deesoft.eatsout.event.FollowCreatedEvent;
import pl.deesoft.eatsout.event.FollowDeletedEvent;
import pl.deesoft.eatsout.event.LocationUpdatedEvent;
import pl.deesoft.eatsout.event.LogoutRequestEvent;
import pl.deesoft.eatsout.event.PasswordAuthenticatedEvent;
import pl.deesoft.eatsout.event.PhotoListLoadedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.RestaurantListLoadedEvent;
import pl.deesoft.eatsout.event.TagListLoadedEvent;
import pl.deesoft.eatsout.event.UserGetRequestEvent;
import pl.deesoft.eatsout.event.UserGettedEvent;
import pl.deesoft.eatsout.event.UserListLoadedEvent;
import pl.deesoft.eatsout.repository.AuthenticationRepository;
import pl.deesoft.eatsout.repository.FacebookRepository;
import pl.deesoft.eatsout.repository.PositionRepository;
import pl.deesoft.eatsout.repository.ResourceRepository;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import retrofit.mime.TypedFile;

/**
 * Created by Piotr Krawczyk on 07.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class MyApplication extends Application {
    EventBus eventBus;
    RestClient restClient;
    AuthenticationRepository authenticationRepository;
    FacebookRepository facebookRepository;
    ResourceRepository resourceRepository;
    PositionRepository positionRepository;
    CallbackManager callbackManager;

    LatLng currentPosition;

    @Override
    public void onCreate() {
        super.onCreate();

        eventBus = EventBus.getDefault();

        restClient = new RestClient(getApplicationContext());
        authenticationRepository = new AuthenticationRepository(restClient.getAuthentication());
        facebookRepository = new FacebookRepository();
        resourceRepository = new ResourceRepository(restClient.getApiService(), restClient.getPaginatedApiService());
        positionRepository = new PositionRepository(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        ImageLoaderConfiguration config = Configuration.getDefaultInitOptions(getApplicationContext());
        ImageLoader.getInstance().init(config);

        FacebookSdk.sdkInitialize(getApplicationContext());

        eventBus.register(authenticationRepository);
        eventBus.register(facebookRepository);
        eventBus.register(resourceRepository);
        eventBus.register(positionRepository);
        eventBus.register(QueryRegistry.getInstance());
        eventBus.register(this);
    }

    public void onEvent(ActivityRedirectEvent activityRedirectEvent) {
        startActivity(activityRedirectEvent);
    }

    public void onEvent(final PasswordAuthenticatedEvent passwordAuthenticatedEvent) {
        User currentUser = passwordAuthenticatedEvent.getUser();
        Hawk.put(Constants.SP_TOKEN, passwordAuthenticatedEvent.getToken());
        Hawk.put(Constants.SP_EXPIRATION, new Date().getTime() + Constants.TOKEN_TTL * 1000);
        Hawk.put(Constants.CURRENT_USER, currentUser);

        if (passwordAuthenticatedEvent.isRegistered()) {
            AccountUpdateRequestEvent accountUpdateRequestEvent = new AccountUpdateRequestEvent(passwordAuthenticatedEvent.getUser().getId());
            accountUpdateRequestEvent.setFirstname((String) Hawk.get(Constants.FACEBOOK_FIRSTNAME));
            accountUpdateRequestEvent.setLastname((String) Hawk.get(Constants.FACEBOOK_LASTNAME));
            EventBus.getDefault().post(accountUpdateRequestEvent);

            if (passwordAuthenticatedEvent.getUser() != null || passwordAuthenticatedEvent.getUser().getAvatar() == null) {
                ImageLoader.getInstance().loadImage((String) Hawk.get(Constants.FACEBOOK_IMAGE), Configuration.getAvatarImageOptions(), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        AvatarCreateRequestEvent avatarCreateRequestEvent = new AvatarCreateRequestEvent(passwordAuthenticatedEvent.getUser().getId());
                        avatarCreateRequestEvent.setFile(new TypedFile("image/jpg", ImageLoader.getInstance().getDiskCache().get((String) Hawk.get(Constants.FACEBOOK_IMAGE))));
                        EventBus.getDefault().post(avatarCreateRequestEvent);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
            }
        }

        EventBus.getDefault().post(new UserGetRequestEvent(currentUser.getId(), true));
        startActivity(new ActivityRedirectEvent(MainActivity.class));
    }

    public void onEvent(AvatarCreatedEvent avatarCreatedEvent) {
        User user = avatarCreatedEvent.getUser();
        user.setAvatar(new Avatar(avatarCreatedEvent.getId(), avatarCreatedEvent.getFile()));
        Hawk.put(Constants.CURRENT_USER, user);
    }

    public void onEvent(AvatarUpdatedEvent avatarUpdatedEvent) {
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    public void onEvent(ApplicationReadyEvent applicationReadyEvent) {
        long expired_at = Hawk.get(Constants.SP_EXPIRATION, Long.valueOf(-1));
        String token = Hawk.get(Constants.SP_TOKEN, null);
        User user = Hawk.get(Constants.CURRENT_USER);

        if (!(expired_at < 0 || expired_at < new Date().getTime()) && token != null && user != null) {
            EventBus.getDefault().post(new PasswordAuthenticatedEvent(user, token));
        } else {
            startActivity(new ActivityRedirectEvent(LoginActivity.class));
        }
    }

    public void onEvent(LogoutRequestEvent logoutRequestEvent) {
        Hawk.remove(Constants.SP_TOKEN);
        Hawk.remove(Constants.SP_EXPIRATION);
        Hawk.remove(Constants.CURRENT_USER);
        Hawk.remove(Constants.FACEBOOK_IMAGE);

        //todo uncomment
        LoginManager.getInstance().logOut();

        startActivity(new ActivityRedirectEvent(LoginActivity.class));
    }

    @DebugLog
    public void onEvent(LocationUpdatedEvent locationUpdatedEvent) {
        if (locationUpdatedEvent.getLatLng() != null) {
            currentPosition = locationUpdatedEvent.getLatLng();
        }
    }

    @DebugLog
    public void onEventBackgroundThread(TagListLoadedEvent tagListLoadedEvent) {
        if (tagListLoadedEvent.getTags() != null && tagListLoadedEvent.getTags().size() > 0) {
            Hawk.put(Constants.TAGS_LIST, tagListLoadedEvent.getTags());
        }
    }

    @DebugLog
    public void onEventBackgroundThread(CategoryListLoadedEvent categoryListLoadedEvent) {
        if (categoryListLoadedEvent.getCategories() != null && categoryListLoadedEvent.getCategories().size() > 0) {
            Hawk.put(Constants.CATEGORIES_LIST, categoryListLoadedEvent.getCategories());
        }
    }

    @DebugLog
    public void onEventBackgroundThread(PhotoListLoadedEvent photoListLoadedEvent) {
        if (photoListLoadedEvent.getPhotos() != null && photoListLoadedEvent.getPhotos().size() > 0) {
            if (photoListLoadedEvent.getCacheKey() != null) {
                Hawk.put(photoListLoadedEvent.getCacheKey(), photoListLoadedEvent.getPhotos());
            } else {
                Hawk.put(Constants.PHOTOS_LIST, photoListLoadedEvent.getPhotos());
            }
        }
    }

    @DebugLog
    public void onEventBackgroundThread(RestaurantListLoadedEvent restaurantListLoadedEvent) {
        if (restaurantListLoadedEvent.getRestaurants() != null && restaurantListLoadedEvent.getRestaurants().size() > 0) {
            Hawk.put(Constants.RESTAURANTS_LIST, restaurantListLoadedEvent.getRestaurants());
        }
    }

    @DebugLog
    public void onEventBackgroundThread(UserListLoadedEvent userListLoadedEvent) {
        if (userListLoadedEvent.getUsers() != null && userListLoadedEvent.getUsers().size() > 0) {
            Hawk.put(Constants.USERS_LIST, userListLoadedEvent.getUsers());
        }
    }

    public void onEventBackgroundThread(UserGettedEvent userGettedEvent) {
        if (userGettedEvent.isCurrent()) {
            Hawk.put(Constants.CURRENT_USER, userGettedEvent.getUser());
            EventBus.getDefault().post(new CurrentUserUpdatedEvent(userGettedEvent.getUser()));
        }
    }

    @DebugLog
    public void onEventBackgroundThread(CurrentUserUpdatedEvent currentUserUpdatedEvent) {
        Hawk.put(Constants.CURRENT_USER, currentUserUpdatedEvent.getUser());
    }

    @DebugLog
    public void onEventBackgroundThread(FollowDeletedEvent followDeletedEvent) {
        User currentUser = Hawk.get(Constants.CURRENT_USER);
        currentUser.getFollowing().remove(followDeletedEvent.getFollow());
        EventBus.getDefault().post(new CurrentUserUpdatedEvent(currentUser));
    }

    @DebugLog
    public void onEventBackgroundThread(FollowCreatedEvent followCreatedEvent) {
        User currentUser = Hawk.get(Constants.CURRENT_USER);
        currentUser.getFollowing().add(followCreatedEvent.getFollow());
        EventBus.getDefault().post(new CurrentUserUpdatedEvent(currentUser));
    }

    private void startActivity(ActivityRedirectEvent activityRedirectEvent) {
        Intent intent = new Intent(this, activityRedirectEvent.getCls());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        eventBus.post(new ActivityFinishRequestEvent());
    }
}
