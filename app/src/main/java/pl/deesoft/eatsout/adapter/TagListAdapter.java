package pl.deesoft.eatsout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Tag;

/**
 * Created by Piotr Krawczyk on 18.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class TagListAdapter extends ArrayAdapter<Tag> {
    Context context;
    List<Tag> tags;
    ListView listView;
    int row;

    public TagListAdapter(Context context, int resource, List<Tag> tags, ListView listView) {
        super(context, resource, tags);
        this.context = context;
        this.tags = tags;
        this.listView = listView;
        this.row = resource;
    }

    @Override
    public int getCount() {
        return tags != null ? tags.size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        if ((tags == null) || ((position + 1) > tags.size())) {
            return view;
        }

        final Tag tag = tags.get(position);

        holder.name = (TextView)view.findViewById(R.id.tagName);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (tag != null) {
            if (holder.name != null) {
                holder.name.setText(tag.getName());
            }
        }
        return view;
    }

    public void replaceData(List<Tag> tags) {
        this.tags = tags;
    }

    private static class ViewHolder {
        public TextView name;
    }
}
