package pl.deesoft.eatsout.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.fragment.PhotoViewFragment;

/**
 * Created by Piotr Krawczyk on 23.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PhotoSliderAdapter extends FragmentStatePagerAdapter {

    List<Photo> photos;

    public PhotoSliderAdapter(FragmentManager fragmentManager, List<Photo> photos) {
        super(fragmentManager);
        this.photos = photos;
    }

    @Override
    public Fragment getItem(int position) {
        if (photos != null && photos.size() > position) {
            return PhotoViewFragment.newInstance(photos.get(position));
        }

        return null;
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (photos != null && photos.size() > position && photos.get(position).getTitle() != null) {
            return photos.get(position).getTitle();
        }

        return "Photo " + (position + 1);
    }
}
