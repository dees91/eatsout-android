package pl.deesoft.eatsout.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tokenautocomplete.FilteredArrayAdapter;

import java.util.List;

import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Tag;

/**
 * Created by Piotr Krawczyk on 23.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class TagAutocompleteAdapter extends FilteredArrayAdapter<Tag> {

    public TagAutocompleteAdapter(Context context, int resource, List<Tag> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = l.inflate(R.layout.entry_tag, parent, false);
        }

        Tag tag = getItem(position);

        ((TextView)convertView.findViewById(R.id.name)).setText(tag.getName());

        if (tag.getSlug() != null) {
            ((TextView)convertView.findViewById(R.id.slug)).setText(tag.getSlug());
        }

        return convertView;
    }

    @Override
    protected boolean keepObject(Tag tag, String mask) {
        mask = mask.toLowerCase();

        if (tag.getName() != null) {
            String name = tag.getName();

            return tag.getName().toLowerCase().startsWith(mask);
        }

        return false;
    }
}
