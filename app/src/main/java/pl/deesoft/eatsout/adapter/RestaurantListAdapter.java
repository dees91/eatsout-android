package pl.deesoft.eatsout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Restaurant;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.fragment.RestaurantViewFragment;
import pl.deesoft.eatsout.util.Configuration;

/**
 * Created by Piotr Krawczyk on 23.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class RestaurantListAdapter extends ArrayAdapter<Restaurant> {
    Context context;
    List<Restaurant> restaurants;
    ListView listView;
    int row;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    public RestaurantListAdapter(Context context, int resource, List<Restaurant> restaurants, ListView listView) {
        super(context, resource, restaurants);
        this.context = context;
        this.restaurants = restaurants;
        this.listView = listView;
        this.row = resource;

        imageOptions = Configuration.getThumbImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return restaurants != null ? restaurants.size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        if ((restaurants == null) || ((position + 1) > restaurants.size())) {
            return view;
        }

        final Restaurant restaurant = restaurants.get(position);

        holder.cover = (ImageView)view.findViewById(R.id.cover);
        holder.name = (TextView)view.findViewById(R.id.restaurantName);
        holder.category = (TextView)view.findViewById(R.id.category);
        holder.distance = (TextView)view.findViewById(R.id.distance);
        holder.address = (TextView)view.findViewById(R.id.address);
        holder.rate = (TextView)view.findViewById(R.id.rate);

        view.setOnClickListener(setOnClickListener(restaurant));

        if (restaurant != null) {
            holder.name.setText(restaurant.getName());
            if (restaurant.getCategories() != null && restaurant.getCategories().size() > 0) {
                holder.category.setText(restaurant.getCategories().get(0).getName());
            }
            holder.distance.setText(String.format("%.1f km", restaurant.getDistance()));
            holder.address.setText(restaurant.getAddress());
            holder.rate.setText(restaurant.getRate() > 0 ? String.format("%.1f", restaurant.getRate()) : "-");

            imageLoader.displayImage(restaurant.getCover() != null ? restaurant.getCover() : null, holder.cover, imageOptions);
        }
        return view;
    }

    public void replaceData(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    protected View.OnClickListener setOnClickListener(final Restaurant restaurant) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SwitchFragmentEvent(RestaurantViewFragment.newInstance(restaurant)));
            }
        };
    }

    protected static class ViewHolder {
        public ImageView cover;
        public TextView name;
        public TextView category;
        public TextView distance;
        public TextView address;
        public TextView rate;
    }
}
