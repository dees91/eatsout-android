package pl.deesoft.eatsout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Opinion;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.DateFormat;

/**
 * Created by Piotr Krawczyk on 18.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class OpinionListAdapter extends ArrayAdapter<Opinion> {
    Context context;
    List<Opinion> opinions;
    ListView listView;
    int row;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    public OpinionListAdapter(Context context, int resource, List<Opinion> opinions, ListView listView) {
        super(context, resource, opinions);
        this.context = context;
        this.opinions = opinions;
        this.listView = listView;
        this.row = resource;

        imageOptions = Configuration.getAvatarImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return opinions != null ? opinions.size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        if ((opinions == null) || ((position + 1) > opinions.size())) {
            return view;
        }

        final Opinion opinion = opinions.get(position);

        holder.comment = (TextView)view.findViewById(R.id.comment);
        holder.username = (TextView)view.findViewById(R.id.username);
        holder.date = (TextView)view.findViewById(R.id.date);
        holder.rate = (TextView)view.findViewById(R.id.rate);
        holder.avatar = (ImageView)view.findViewById(R.id.avatar);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (opinion != null) {
            holder.comment.setText(opinion.getComment());
            holder.username.setText(opinion.getUser().getUsername());
            holder.date.setText(DateFormat.format(opinion.getDate()));
            holder.rate.setText(String.format("%.1f", opinion.getRate()));
            imageLoader.displayImage(opinion.getUser().getAvatar() != null ? opinion.getUser().getAvatar().getFile() : null, holder.avatar, imageOptions);
        }
        return view;
    }

    public void replaceData(List<Opinion> opinions) {
        this.opinions = opinions;
    }

    private static class ViewHolder {
        public TextView comment;
        public ImageView avatar;
        public TextView username;
        public TextView date;
        public TextView rate;
    }
}
