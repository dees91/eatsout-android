package pl.deesoft.eatsout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.fragment.PhotoSliderFragment;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.util.UserProfile;

/**
 * Created by Piotr Krawczyk on 22.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PhotoGridAdapter extends BaseAdapter {
    private List<Photo> photos;
    private LayoutInflater layoutInflater;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    public PhotoGridAdapter(Context context, List<Photo> photos) {
        layoutInflater = LayoutInflater.from(context);

        this.photos = photos;

        imageOptions = Configuration.getDefaultImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Photo getItem(int position) {
        return photos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return photos.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ImageView picture;
        TextView name;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.grid_item, viewGroup, false);
            view.setTag(R.id.picture, view.findViewById(R.id.picture));
            view.setTag(R.id.text, view.findViewById(R.id.text));
        }

        picture = (ImageView) view.getTag(R.id.picture);
        name = (TextView) view.getTag(R.id.text);

        Photo photo = getItem(position);

        imageLoader.displayImage(Constants.WEBROOT_URL + photo.getFile(), picture, imageOptions);
        name.setText(buildText(photo));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SwitchFragmentEvent(PhotoSliderFragment.newInstance(photos, position)));
            }
        });

        return view;
    }

    public void replaceData(List<Photo> photos) {
        this.photos = photos;
    }

    private String buildText(Photo photo) {
        return photo.getRestaurant().getName() + " added by " + UserProfile.getUsername(photo.getUser());
    }
}
