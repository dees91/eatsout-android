package pl.deesoft.eatsout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.orhanobut.hawk.Hawk;

import java.util.HashMap;
import java.util.List;

import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Follow;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.CurrentUserUpdatedEvent;
import pl.deesoft.eatsout.event.FollowCreateRequestEvent;
import pl.deesoft.eatsout.event.FollowCreatedEvent;
import pl.deesoft.eatsout.event.FollowDeleteRequestEvent;
import pl.deesoft.eatsout.event.FollowDeletedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.fragment.UserViewFragment;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.util.UserProfile;

/**
 * Created by Piotr Krawczyk on 18.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class UserListAdapter extends ArrayAdapter<User> {
    Context context;
    List<User> users;
    ListView listView;
    User currentUser;
    HashMap<Long, Boolean> status;
    int row;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    public UserListAdapter(Context context, int resource, List<User> users, ListView listView) {
        super(context, resource, users);
        this.context = context;
        this.users = users;
        this.listView = listView;
        this.row = resource;

        status = new HashMap<>();

        imageOptions = Configuration.getAvatarImageOptions();
        imageLoader = ImageLoader.getInstance();
        currentUser = Hawk.get(Constants.CURRENT_USER);
        for (User user : users) {
            Follow follow = currentUser.getFollowByUser(user);
            status.put(user.getId(), follow != null);
        }
        EventBus.getDefault().register(this);
    }

    @Override
    public int getCount() {
        return users != null ? users.size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        if ((users == null) || ((position + 1) > users.size())) {
            return view;
        }

        final User user = users.get(position);

        holder.avatar = (RoundedImageView)view.findViewById(R.id.avatar);
        holder.name = (TextView)view.findViewById(R.id.username);
        holder.follow = (CheckBox)view.findViewById(R.id.follow);
        holder.photos = (TextView)view.findViewById(R.id.photos);
        holder.opinions = (TextView)view.findViewById(R.id.opinions);
        holder.following = (TextView)view.findViewById(R.id.following);
        holder.followed = (TextView)view.findViewById(R.id.followed);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SwitchFragmentEvent(UserViewFragment.newInstance(user)));
            }
        });

        if (user != null) {

            holder.name.setText(UserProfile.getUsername(user));
            holder.photos.setText("Added " + user.getPhotos() + " photos,");
            holder.opinions.setText("rated " + user.getOpinions() + " places");
            List<Follow> followed = user.getFollowing();
            if (followed != null && followed.size() > 0) {
                holder.following.setText("Following " + followed.size() + " users,");
            }
            holder.followed.setText(user.getFollowed() + " followers");

            imageLoader.displayImage(user.getAvatar() != null ? user.getAvatar().getFile() : null, holder.avatar, imageOptions);

            holder.follow.setChecked(status.get(user.getId()));

            holder.follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Follow follow = currentUser.getFollowByUser(user);
                    if (holder.follow.isChecked()) {
                        EventBus.getDefault().post(new ProgressRequestEvent());
                        EventBus.getDefault().post(new FollowCreateRequestEvent(currentUser.getId(), user.getId()));
                        status.put(user.getId(), true);
                    } else {
                        if (follow != null) {
                            EventBus.getDefault().post(new ProgressRequestEvent());
                            EventBus.getDefault().post(new FollowDeleteRequestEvent(follow));
                        }
                        status.put(user.getId(), false);
                    }
                }
            });
        }

        return view;
    }

    public void onEvent(FollowDeletedEvent followDeletedEvent) {
        int index = users.indexOf(followDeletedEvent.getFollow().getFollowed());
        EventBus.getDefault().post(new ProgressEndEvent());
        users.get(index).setFollowed(users.get(index).getFollowed() - 1);
        //todo
        Hawk.put(Constants.USERS_LIST, users);
    }

    public void onEvent(FollowCreatedEvent followCreatedEvent) {
        int index = users.indexOf(followCreatedEvent.getFollow().getFollowed());
        EventBus.getDefault().post(new ProgressEndEvent());
        users.get(index).setFollowed(users.get(index).getFollowed() + 1);
        //todo
        Hawk.put(Constants.USERS_LIST, users);
    }

    public void onEvent(CurrentUserUpdatedEvent currentUserUpdatedEvent) {
        currentUser = currentUserUpdatedEvent.getUser();
    }

    public void replaceData(List<User> users) {
        this.users = users;
        status.clear();
        for (User user : users) {
            Follow follow = currentUser.getFollowByUser(user);
            status.put(user.getId(), follow != null);
        }
    }

    private static class ViewHolder {
        public RoundedImageView avatar;
        public TextView name;
        public CheckBox follow;
        public TextView photos;
        public TextView opinions;
        public TextView following;
        public TextView followed;
    }
}
