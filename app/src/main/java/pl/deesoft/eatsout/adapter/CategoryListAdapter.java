package pl.deesoft.eatsout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Category;

/**
 * Created by Piotr Krawczyk on 18.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class CategoryListAdapter extends ArrayAdapter<Category> {
    Context context;
    List<Category> categories;
    ListView listView;
    int row;

    public CategoryListAdapter(Context context, int resource, List<Category> categories, ListView listView) {
        super(context, resource, categories);
        this.context = context;
        this.categories = categories;
        this.listView = listView;
        this.row = resource;
    }

    @Override
    public int getCount() {
        return categories != null ? categories.size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        if ((categories == null) || ((position + 1) > categories.size())) {
            return view;
        }

        final Category category = categories.get(position);

        holder.name = (TextView)view.findViewById(R.id.categoryName);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (category != null) {
            if (holder.name != null) {
                holder.name.setText(category.getName());
            }
        }
        return view;
    }

    public void replaceData(List<Category> categories) {
        this.categories = categories;
    }

    private static class ViewHolder {
        public TextView name;
    }
}
