package pl.deesoft.eatsout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Photo;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.fragment.PhotoSliderFragment;
import pl.deesoft.eatsout.util.Configuration;
import pl.deesoft.eatsout.util.DateFormat;

/**
 * Created by Piotr Krawczyk on 18.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PhotoListAdapter extends ArrayAdapter<Photo> {
    Context context;
    List<Photo> photos;
    ListView listView;
    int row;
    private final DisplayImageOptions imageOptions;
    private final ImageLoader imageLoader;

    public PhotoListAdapter(Context context, int resource, List<Photo> photos, ListView listView) {
        super(context, resource, photos);
        this.context = context;
        this.photos = photos;
        this.listView = listView;
        this.row = resource;

        imageOptions = Configuration.getThumbImageOptions();
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return photos != null ? photos.size() : 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        if ((photos == null) || ((position + 1) > photos.size())) {
            return view;
        }

        final Photo photo = photos.get(position);

        holder.name = (TextView)view.findViewById(R.id.title);
        holder.restaurant = (TextView)view.findViewById(R.id.restaurant);
        holder.date = (TextView)view.findViewById(R.id.date);
        holder.image = (RoundedImageView)view.findViewById(R.id.photo);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SwitchFragmentEvent(PhotoSliderFragment.newInstance(photos, position)));
            }
        });

        if (photo != null) {
            if (photo.getTitle() != null) {
                holder.name.setText(photo.getTitle());
            } else {
                holder.name.setVisibility(View.GONE);
            }
            holder.restaurant.setText(photo.getRestaurant().getName());
            holder.date.setText(DateFormat.format(photo.getDate()));

            imageLoader.displayImage(photo.getFile() != null ? photo.getFile() : null, holder.image, imageOptions);
        }
        return view;
    }

    public void replaceData(List<Photo> photos) {
        this.photos = photos;
    }

    private static class ViewHolder {
        public TextView name;
        public TextView restaurant;
        public TextView date;
        public RoundedImageView image;
    }
}
