package pl.deesoft.eatsout.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piotr Krawczyk on 27.04.15.
 * krawczyk.piotr.91@gmail.com
 */
public class Constants {
    public final static String WEBROOT_URL = "";
    public final static String API_BASE_URL = "http://eatsout.deesoft.pl/web/api";
    public final static String API_TOKEN_QUERY_NAME = "bearer";
    public final static String API_AUTH_URL = "/auth/";
    public final static String API_RESET_PASSWORD = "/auth/reset/";
    public final static String API_REGISTER = "/auth/register/";
    public final static String API_CHANGE_PASSWORD = "/auth/change/";
    public final static String API_RESTAURANT_URL = "/restaurants/";
    public final static String API_OPINION_URL = "/opinions/";
    public final static String API_FOLLOW_URL = "/follows/";
    public final static String API_PHOTOS_URL = "/photos/";
    public final static String API_TAG_URL = "/tags/";
    public final static String API_CATEGORY_URL = "/categories/";
    public final static String API_USER_URL = "/api_users/";
    public final static String API_AVATAR_URL = "/avatars/";

    public final static long TOKEN_TTL = 15768000;
    public final static String SP_TOKEN = "SP_TOKEN";
    public final static String SP_EXPIRATION = "SP_EXPIRATION";
    public final static String CURRENT_USER = "CURRENT_USER";
    public final static String TAGS_LIST = "TAGS_LIST";
    public final static String CATEGORIES_LIST = "CATEGORIES_LIST";
    public final static String USERS_LIST = "USERS_LIST";
    public final static String USER_SINGLE = "USER_SINGLE";
    public final static String RESTAURANTS_LIST = "RESTAURANTS_LIST";
    public final static String RESTAURANT_SINGLE = "RESTAURANT_SINGLE";
    public final static String PHOTOS_LIST = "PHOTOS_LIST";
    public final static String OPINIONS_LIST = "OPINIONS_LIST";
    public final static String RESTAURANT_PHOTOS_LIST = Constants.PHOTOS_LIST + "_RESTAURANT_#";
    public final static String USER_PHOTOS_LIST = Constants.PHOTOS_LIST + "_USER_#";
    public final static String USER_OPINIONS_LIST = Constants.OPINIONS_LIST + "_USER_#";
    public final static String PHOTO_SINGLE = "PHOTO_SINGLE";
    public final static String PHOTO_POSITION = "PHOTO_POSITION";
    public final static String CAMERA_POSITION = "CAMERA_POSITION";
    public final static String MY_LOCATION = "MY_LOCATION";

    public final static String PICTURE_URI = "PICTURE_URI";

    public final static int REQUEST_CODE_IMAGE_FROM_GALLERY = 88;
    public final static int REQUEST_CODE_AVATAR_FROM_GALLERY = 77;
    public final static int REQUEST_CODE_IMAGE_FROM_CAMERA = 99;
    public final static int REQUEST_CODE_AVATAR_FROM_CAMERA = 66;

    public final static String FACEBOOK_IMAGE = "FACEBOOK_IMAGE";
    public final static String FACEBOOK_FIRSTNAME = "FACEBOOK_FIRSTNAME";
    public final static String FACEBOOK_LASTNAME = "FACEBOOK_LASTNAME";
    public final static String FACEBOOK_IMAGE_URL = "https://graph.facebook.com/v2.0/{ID}/picture?width=512&height=512";

    public final static String FACEBOOK_APP_ID = "1572485783034605";
    public final static String FACEBOOK_APP_NAMESPACE = "dupka_qwfvs";
    public final static List<String> FACEBOOK_PERMISSIONS = new ArrayList<String>() {{
        add("public_profile");
        add("email");
        add("user_birthday");
        add("user_friends");
    }};

    public final static String HAWK_PASSWORD = "eats_hawk1234";
}
