package pl.deesoft.eatsout.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Piotr Krawczyk on 06.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class DateFormat {
    public static String format(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return dateFormat.format(date);
    }

    public static String currentDate() {
        return format(new Date());
    }
}
