package pl.deesoft.eatsout.util;

import android.content.Context;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import pl.deesoft.eatsout.R;

/**
 * Created by Piotr Krawczyk on 23.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class Configuration {
    public static int MAP_ZOOM = 14;
    public static int UPDATE_DISTANCE = 200;
    public static long QUERY_DIFF = 120 * 1000;

    public static DisplayImageOptions getDefaultImageOptions() {
        return new DisplayImageOptions.Builder()
            .showImageOnFail(R.drawable.default_placeholder)
            .showImageForEmptyUri(R.drawable.default_placeholder)
            .showImageOnLoading(R.drawable.image_loading)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .displayer(new FadeInBitmapDisplayer(0))
            .imageScaleType(ImageScaleType.EXACTLY)
            .build();
    }

    public static DisplayImageOptions getAvatarImageOptions() {
        return new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.user_avatar)
                .showImageOnFail(R.drawable.user_avatar)
                .showImageOnLoading(R.drawable.image_loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static DisplayImageOptions getThumbImageOptions() {
        return new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.default_placeholder)
                .showImageOnFail(R.drawable.default_placeholder)
                .showImageOnLoading(R.drawable.image_loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static ImageLoaderConfiguration getDefaultInitOptions(Context context) {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        final int cacheSize = maxMemory / 10;

        return new ImageLoaderConfiguration.Builder(context)
            .threadPriority(Thread.NORM_PRIORITY - 2)
            .denyCacheImageMultipleSizesInMemory()
            .diskCacheFileNameGenerator(new Md5FileNameGenerator())
            .diskCacheSize(100 * 1024 * 1024)
            .tasksProcessingOrder(QueueProcessingType.LIFO)
            .memoryCacheSize(cacheSize)
            //todo remove this in production
            .writeDebugLogs()
            .build();
    }
}
