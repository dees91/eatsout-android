package pl.deesoft.eatsout.util;

import pl.deesoft.eatsout.entity.User;

/**
 * Created by Piotr Krawczyk on 04.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class UserProfile {
    public static String getUsername(User user) {
        if (user.getFirstname() != null && user.getLastname() != null) {
            return (user.getFirstname() + " " + user.getLastname());
        } else if (user.getFirstname() != null) {
            return user.getFirstname();
        } else {
            return user.getUsername();
        }
    }

    public static boolean hasCityOrCountry(User user) {
        if (user.getCity() == null && user.getCountry() == null) {
            return false;
        }

        return true;
    }

    public static String getCity(User user) {
        String name = "";

        if (user.getCity() != null && user.getCountry() != null) {
            name += user.getCity() + ", " + user.getCountry();
        } else if (user.getCity() != null) {
            name += user.getCity();
        } else {
            name += user.getCountry();
        }

        return name;
    }
}
