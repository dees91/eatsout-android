package pl.deesoft.eatsout.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Piotr Krawczyk on 15.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class File {
    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static Bitmap getBitmapFromUri(Context context, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();

        return image;
    }

    public static Uri getImageUri(Context context, Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);

        return Uri.parse(path);
    }

    public static java.io.File getOutputTempFile(String dir, String ext) {
        String root = Environment.getExternalStorageDirectory().toString();
        java.io.File videos_dir = new java.io.File(root + "/Android/data/pl.deesoft.eatsout/" + dir);

        if (!videos_dir.exists() && !videos_dir.mkdirs()) {
            return null;
        }

        return new java.io.File(videos_dir.getPath(), "eatsout_TEMP." + ext);
    }

    public static Uri getOutputTempUri(String dir, String ext) {
        java.io.File temp = getOutputTempFile(dir, ext);
        return temp == null ? null : Uri.fromFile(temp);
    }

    public static Uri getOutputTempUri(String dir) {
        return  getOutputTempUri(dir, "jpg");
    }

    public static Uri renameFile(String dir, String ext) {
        java.io.File tempFile = getOutputTempFile(dir, ext);

        if (tempFile != null) {
            java.io.File newFile = new java.io.File(tempFile.getPath().replace("eatsout_TEMP", "eatsout_" + new Date().getTime()));

            return Uri.fromFile(resizeToMax(tempFile, newFile, 1024, 1024));
        }

        return null;
    }

    public static java.io.File resizeToMax(java.io.File tempFile, java.io.File newFile, int maxWidth, int maxHeight) {
        java.io.File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        Bitmap bitmap = BitmapFactory.decodeFile(tempFile.getPath());

        int currentWidth = bitmap.getWidth();
        int currentHeight = bitmap.getHeight();

        double ratioX = (double)maxWidth / currentWidth;
        double ratioY = (double)maxHeight / currentHeight;
        double ratio = Math.min(ratioX, ratioY);

        int newWidth = (int)(currentWidth * ratio);
        int newHeight = (int)(currentHeight * ratio);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);

        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(newFile);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            bitmap.recycle();
            scaledBitmap.recycle();

            return newFile;
        } catch (Exception e) {}

        return tempFile;
    }
}
