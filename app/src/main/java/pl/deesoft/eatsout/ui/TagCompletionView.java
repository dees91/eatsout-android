package pl.deesoft.eatsout.ui;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tokenautocomplete.TokenCompleteTextView;

import pl.deesoft.eatsout.R;
import pl.deesoft.eatsout.entity.Tag;

/**
 * Created by Piotr Krawczyk on 23.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class TagCompletionView extends TokenCompleteTextView {

    public TagCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject(Object object) {
        Tag tag = (Tag) object;

        LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout)layoutInflater.inflate(R.layout.token_tag, (ViewGroup) TagCompletionView.this.getParent(), false);
        ((TextView)view.findViewById(R.id.tagName)).setText(tag.getName());

        return view;
    }

    @Override
    protected Object defaultObject(String completionText) {
        Tag tag = new Tag();
        tag.setName(completionText);
        tag.setSlug(completionText.toLowerCase());

        return tag;
    }
}
