package pl.deesoft.eatsout.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.AccessToken;
import com.facebook.login.widget.LoginButton;

/**
 * Created by Piotr Krawczyk on 02.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class FacebookLogin extends LoginButton {
    public FacebookLogin(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void configureButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super.configureButton(context, attrs, defStyleAttr, defStyleRes);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken != null) {
            setInternalOnClickListener(null);
        }
    }
}
