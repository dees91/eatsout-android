package pl.deesoft.eatsout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.orhanobut.hawk.Hawk;
import com.soundcloud.android.crop.Crop;

import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import hugo.weaving.DebugLog;
import pl.deesoft.eatsout.api.ApiService;
import pl.deesoft.eatsout.api.RestClient;
import pl.deesoft.eatsout.api.model.RestError;
import pl.deesoft.eatsout.event.AccountUpdatedEvent;
import pl.deesoft.eatsout.event.ActivityFinishRequestEvent;
import pl.deesoft.eatsout.event.AvatarSelectedEvent;
import pl.deesoft.eatsout.event.PhotoCameraRequestEvent;
import pl.deesoft.eatsout.event.PhotoCreatedEvent;
import pl.deesoft.eatsout.event.PhotoGalleryRequestEvent;
import pl.deesoft.eatsout.event.PhotoSelectedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.RestaurantSelectRequestEvent;
import pl.deesoft.eatsout.event.SwitchFragmentEvent;
import pl.deesoft.eatsout.fragment.BaseFragment;
import pl.deesoft.eatsout.fragment.MainFragment;
import pl.deesoft.eatsout.fragment.MenuFragment;
import pl.deesoft.eatsout.fragment.MenuTitleFragment;
import pl.deesoft.eatsout.fragment.PhotoViewFragment;
import pl.deesoft.eatsout.fragment.RestaurantSelectFragment;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.util.File;


public class MainActivity extends ActionBarActivity {
    MyApplication myApplication;
    RestClient restClient;
    ApiService apiService;

    BaseFragment currentMainFragment = null;
    String currentFragmentTag = null;
    ActionBarDrawerToggle actionBarDrawerToggle = null;
    LocationManager locationManager = null;

    @InjectView(R.id.toolbar) Toolbar toolbar;
    @InjectView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @InjectView(R.id.left_drawer) View leftView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        myApplication = (MyApplication) getApplication();

        restClient = new RestClient(getApplicationContext());
        apiService = restClient.getApiService();

        if (savedInstanceState == null) {
            currentMainFragment = MainFragment.newInstance();
            currentFragmentTag = currentMainFragment.getClass().getName();
            getSupportFragmentManager().beginTransaction().add(R.id.main_container, currentMainFragment, currentFragmentTag).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.menu_container, MenuFragment.newInstance(), MenuFragment.TAG).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.toolbar_menu_container, MenuTitleFragment.newInstance(), MenuTitleFragment.TAG).commit();
        } else {
            currentFragmentTag = savedInstanceState.getString("currentFragmentTag");
            currentMainFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("currentFragmentTag", currentFragmentTag);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else {
            new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("Are you really want to leave?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EventBus.getDefault().post(new ActivityFinishRequestEvent());
                    }
                })
                .setNegativeButton("No", null)
                .show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri destination;
            switch (requestCode) {
                case Crop.REQUEST_PICK:
                    destination = Uri.fromFile(new java.io.File(getCacheDir(), "cropped_" + new Date().getTime()));
                    Crop.of(data.getData(), destination).asSquare().withMaxSize(128, 128).start(this); break;
                case Crop.REQUEST_CROP:
                    EventBus.getDefault().post(new AvatarSelectedEvent(Crop.getOutput(data))); break;
                case Constants.REQUEST_CODE_IMAGE_FROM_GALLERY:
                    EventBus.getDefault().post(new PhotoSelectedEvent(data.getData())); break;
                case Constants.REQUEST_CODE_IMAGE_FROM_CAMERA:
                    EventBus.getDefault().post(new PhotoSelectedEvent(File.renameFile("images", "jpg"))); break;
                case Constants.REQUEST_CODE_AVATAR_FROM_GALLERY: break;
                case Constants.REQUEST_CODE_AVATAR_FROM_CAMERA:
                    destination = Uri.fromFile(new java.io.File(getCacheDir(), "cropped_" + new Date().getTime()));
                    Crop.of(File.getOutputTempUri("images", "jpg"), destination).asSquare().start(this); break;
                default: break;
            }
        }
    }

    @DebugLog
    public void onEvent(SwitchFragmentEvent event) {
        currentMainFragment = event.getBaseFragment();
        handleFragmentSwitch(event.isForce());
    }

    public void onEvent(PhotoGalleryRequestEvent photoGalleryRequestEvent) {
        if (photoGalleryRequestEvent.getCode() == Constants.REQUEST_CODE_AVATAR_FROM_GALLERY) {
            Crop.pickImage(this);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, "Select image"), photoGalleryRequestEvent.getCode());
        }
    }

    public void onEvent(PhotoCameraRequestEvent photoCameraRequestEvent) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, File.getOutputTempUri("images"));
        startActivityForResult(intent, photoCameraRequestEvent.getCode());
    }

    public void onEvent(ActivityFinishRequestEvent activityFinishRequestEvent) {
        finish();
    }

    public void onEvent(RestaurantSelectRequestEvent restaurantSelectRequestEvent) {
        currentMainFragment = RestaurantSelectFragment.newInstance();
        handleFragmentSwitch(false);
    }

    public void onEvent(PhotoCreatedEvent photoCreatedEvent) {
        Crouton.makeText(this, "Photo successfully created.", Style.CONFIRM).show();
        EventBus.getDefault().post(new ProgressEndEvent());
        EventBus.getDefault().post(new SwitchFragmentEvent(PhotoViewFragment.newInstance(photoCreatedEvent.getPhoto())));
    }

    public void onEvent(AccountUpdatedEvent accountUpdatedEvent) {
        Crouton.makeText(this, "Account successfully updated.", Style.CONFIRM).show();
        Hawk.put(Constants.CURRENT_USER, accountUpdatedEvent.getUser());
        EventBus.getDefault().post(new ProgressEndEvent());
    }

    public void onEvent(RestError restError) {
        EventBus.getDefault().post(new ProgressEndEvent());
        Crouton.makeText(this, restError.getMessages().get(0), Style.ALERT).show();
    }

    public MyApplication getMyApplication() {
        return myApplication;
    }

    @DebugLog
    private void handleFragmentSwitch(boolean force) {
        String backStateName = currentMainFragment.getClass().getName();
        currentFragmentTag = backStateName;

        FragmentManager fragmentManager = getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);

        if (force || !fragmentPopped && fragmentManager.findFragmentByTag(currentFragmentTag) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_container, currentMainFragment, currentFragmentTag);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commit();
        }

        drawerLayout.closeDrawer(leftView);
    }
}
