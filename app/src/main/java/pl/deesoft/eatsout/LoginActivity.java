package pl.deesoft.eatsout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.orhanobut.hawk.Hawk;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import hugo.weaving.DebugLog;
import pl.deesoft.eatsout.api.model.RestError;
import pl.deesoft.eatsout.entity.User;
import pl.deesoft.eatsout.event.ActivityFinishRequestEvent;
import pl.deesoft.eatsout.event.FacebookProfileRequestEvent;
import pl.deesoft.eatsout.event.PasswordAuthenticatedEvent;
import pl.deesoft.eatsout.event.PasswordAuthenticationRequestEvent;
import pl.deesoft.eatsout.event.PasswordChangeErrorEvent;
import pl.deesoft.eatsout.event.PasswordChangeRequestEvent;
import pl.deesoft.eatsout.event.PasswordChangedEvent;
import pl.deesoft.eatsout.event.PasswordResetErrorEvent;
import pl.deesoft.eatsout.event.PasswordResetRequestEvent;
import pl.deesoft.eatsout.event.PasswordResettedEvent;
import pl.deesoft.eatsout.event.ProgressEndEvent;
import pl.deesoft.eatsout.event.ProgressRequestEvent;
import pl.deesoft.eatsout.event.RegisterErrorEvent;
import pl.deesoft.eatsout.event.RegisterRequestEvent;
import pl.deesoft.eatsout.event.RegisteredEvent;
import pl.deesoft.eatsout.ui.FacebookLogin;
import pl.deesoft.eatsout.util.Constants;
import pl.deesoft.eatsout.ui.ProgressCircular;

public class LoginActivity extends ActionBarActivity {
    Activity activity;
    Context context;
    MyApplication application;
    DialogPlus resetDialog;
    DialogPlus changeDialog;
    DialogPlus registerDialog;
    String resetEmail = null;
    boolean resetPassed = false;

    @InjectView(R.id.inputLogin) EditText username;
    @InjectView(R.id.inputPassword) EditText password;
    @InjectView(R.id.changePasswordButton) TextView changePasswordButton;
    @InjectView(R.id.login_button) FacebookLogin loginButton;
    @InjectView(R.id.progress) ProgressCircular progress;
    @InjectView(R.id.loginWrapper) LinearLayout loginWrapper;

    @OnClick(R.id.buttonLogin) void performLogin() {
        Hawk.remove(Constants.SP_TOKEN);
        EventBus.getDefault().post(new PasswordAuthenticationRequestEvent(username.getText().toString().trim(), password.getText().toString().trim()));
        EventBus.getDefault().post(new ProgressRequestEvent());
        setControlsEnabled(false, loginWrapper);
    }

    @OnClick(R.id.changePasswordButton) void performPasswordChange() {
        if (!resetPassed) {
            resetDialog.show();
        } else {
            changeDialog.show();
        }
    }

    @OnClick(R.id.registerButton) void performRegister() {
        registerDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        activity = this;
        context = this;
        application = (MyApplication)getApplication();

        initDialogs();

        if (AccessToken.getCurrentAccessToken() == null) {
            loginButton.setReadPermissions(Constants.FACEBOOK_PERMISSIONS);
            loginButton.registerCallback(application.callbackManager, application.facebookRepository);
        } else {
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new FacebookProfileRequestEvent(AccessToken.getCurrentAccessToken()));
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        application.callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Crouton.cancelAllCroutons();
        super.onDestroy();
    }

    public void onEvent(RestError restError) {
        EventBus.getDefault().post(new ProgressEndEvent());
        setControlsEnabled(true, loginWrapper);
        Crouton.makeText(this, restError.getMessages().get(0), Style.ALERT).show();
    }

    public void onEvent(PasswordResettedEvent passwordResetEvent) {
        EventBus.getDefault().post(new ProgressEndEvent());
        setControlsEnabled(true, loginWrapper);
        resetPassed = true;
        Crouton.makeText(this, "We sent secret password to given address, please provide it here", Style.CONFIRM).show();
        changeDialog.show();
    }

    public void onEvent(PasswordChangedEvent passwordChangedEvent) {
        EventBus.getDefault().post(new ProgressEndEvent());
        setControlsEnabled(true, loginWrapper);
        Crouton.makeText(this, "Password successfully changed.", Style.CONFIRM).show();
        resetEmail = null;
        resetPassed = false;
    }

    public void onEvent(PasswordAuthenticatedEvent passwordAuthenticatedEvent) {
        EventBus.getDefault().post(new ProgressEndEvent());
        setControlsEnabled(true, loginWrapper);
    }

    @DebugLog
    public void onEvent(RegisteredEvent registeredEvent) {
        EventBus.getDefault().post(new ProgressEndEvent());
        setControlsEnabled(true, loginWrapper);
        Crouton.makeText(this, "Successfully registered. You can login now.", Style.CONFIRM).show();
        username.setText(registeredEvent.getUser().getUsername());
    }

    public void onEvent(RegisterErrorEvent registerErrorEvent) {
        if (!registerDialog.isShowing()) {
            registerDialog.show();
        }
    }

    public void onEvent(PasswordResetErrorEvent passwordResetErrorEvent) {
        if (!resetDialog.isShowing()) {
            resetDialog.show();
        }
    }

    public void onEvent(PasswordChangeErrorEvent passwordChangeErrorEvent) {
        if (!changeDialog.isShowing()) {
            changeDialog.show();
        }
    }

    public void onEvent(ProgressRequestEvent progressRequestEvent) {
        progress.setVisibility(View.VISIBLE);
    }

    public void onEvent(ProgressEndEvent progressEndEvent) {
        progress.setVisibility(View.GONE);
    }

    public void onEvent(FacebookProfileRequestEvent facebookProfileRequestEvent) {
        EventBus.getDefault().post(new ProgressRequestEvent());
        setControlsEnabled(false, loginWrapper);
    }

    public void onEvent(ActivityFinishRequestEvent activityFinishRequestEvent) {
        finish();
    }

    private void initDialogs() {
        resetDialog = new DialogPlus.Builder(this)
            .setContentHolder(new ViewHolder(R.layout.dialog_password_reset))
            .setOnClickListener(new OnPasswordResetClick())
            .setGravity(DialogPlus.Gravity.CENTER)
            .create();

        changeDialog = new DialogPlus.Builder(this)
            .setContentHolder(new ViewHolder(R.layout.dialog_password_change))
            .setOnClickListener(new OnPasswordChangeClick())
            .setGravity(DialogPlus.Gravity.CENTER)
            .create();

        registerDialog = new DialogPlus.Builder(this)
            .setContentHolder(new ViewHolder(R.layout.dialog_register))
            .setOnClickListener(new OnRegisterClick())
            .setGravity(DialogPlus.Gravity.CENTER)
            .create();
    }

    protected void setControlsEnabled(boolean enable, LinearLayout linearLayout) {
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            View child = linearLayout.getChildAt(i);
            if (!(child instanceof ProgressCircular)) {
                child.setEnabled(enable);
            }
        }
    }

    private class OnRegisterClick implements OnClickListener {
        @DebugLog
        @Override
        public void onClick(DialogPlus dialogPlus, View view) {
            if (view.findViewById(R.id.registerButton) instanceof Button) {
                EditText usernameInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.usernameInput);
                EditText passwordInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.passwordInput);
                EditText emailInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.emailInput);
                EditText firstnameInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.firstnameInput);
                EditText lastnameInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.lastnameInput);

                String username = usernameInput.getText().toString().trim();
                String password = passwordInput.getText().toString().trim();
                String email = emailInput.getText().toString().trim();
                String firstname = firstnameInput.getText().toString().trim();
                String lastname = lastnameInput.getText().toString().trim();

                if (username.equals("") || password.equals("") || email.equals("")) {
                    if (username.equals("")) {
                        Crouton.makeText(activity, "Not valid username.", Style.ALERT).show();
                    } else if (password.equals("")) {
                        Crouton.makeText(activity, "Not valid password.", Style.ALERT).show();
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        Crouton.makeText(activity, "Not valid email.", Style.ALERT).show();
                    }
                } else {
                    User user = new User();
                    user.setUsername(username);
                    user.setPassword(password);
                    user.setEmail(email);
                    user.setFirstname(firstname);
                    user.setLastname(lastname);

                    String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

                    user.setPhone_id(android_id);

                    registerDialog.dismiss();
                    EventBus.getDefault().post(new ProgressRequestEvent());
                    setControlsEnabled(false, loginWrapper);
                    EventBus.getDefault().post(new RegisterRequestEvent(user));
                }
            }
        }
    }

    private class OnPasswordChangeClick implements OnClickListener {

        @Override
        public void onClick(DialogPlus dialogPlus, View view) {
            if (view.findViewById(R.id.changeButton) instanceof Button) {
                EditText hashInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.hashInput);
                EditText newPasswordInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.newPassword);
                String hash = hashInput.getText().toString().trim();
                String newPassword = newPasswordInput.getText().toString().trim();
                if (!hash.equals("")) {
                    if (!newPassword.equals("")) {
                        changeDialog.dismiss();
                        EventBus.getDefault().post(new ProgressRequestEvent());
                        setControlsEnabled(false, loginWrapper);
                        EventBus.getDefault().post(new PasswordChangeRequestEvent(resetEmail, hash, newPassword));
                    } else {
                        Crouton.makeText(activity, "New password cannot be empty.", Style.ALERT).show();
                    }
                } else {
                    Crouton.makeText(activity, "Please provide secret hash from email message.", Style.ALERT).show();
                }
            }
        }
    }

    private class OnPasswordResetClick implements OnClickListener {

        @Override
        public void onClick(DialogPlus dialogPlus, View view) {
            if (view.findViewById(R.id.resetButton) instanceof Button) {
                EditText resetInput = (EditText) dialogPlus.getHolderView().findViewById(R.id.resetInput);
                String email = resetInput.getText().toString().trim();
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    resetDialog.dismiss();
                    EventBus.getDefault().post(new ProgressRequestEvent());
                    setControlsEnabled(false, loginWrapper);
                    EventBus.getDefault().post(new PasswordResetRequestEvent(email));
                    resetEmail = email;
                } else {
                    Crouton.makeText(activity, "Invalid email address.", Style.ALERT).show();
                }
            }
        }
    }
}
