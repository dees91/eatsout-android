package pl.deesoft.eatsout.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;

/**
 * Created by Piotr Krawczyk on 28.04.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class Photo {

    @SerializedName("id")
    private long id;

    @SerializedName("file")
    private String file;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("tags")
    private List<Tag> tags;

    @SerializedName("restaurant")
    private Restaurant restaurant;

    @SerializedName("created_at")
    private Date date;

    @SerializedName("user")
    private User user;

    public Photo() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
