package pl.deesoft.eatsout.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 10.06.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class Avatar {
    @SerializedName("id")
    private long id;

    @SerializedName("file")
    private String file;

    public Avatar() {
    }

    public Avatar(long id, String file) {
        this.id = id;
        this.file = file;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return file;
    }
}
