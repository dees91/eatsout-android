package pl.deesoft.eatsout.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by piotrk on 06.05.15.
 */
@Parcel
public class Category {

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("plural_name")
    private String pluralName;

    @SerializedName("short_name")
    private String shortName;

    public Category() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPluralName() {
        return pluralName;
    }

    public void setPluralName(String pluralName) {
        this.pluralName = pluralName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
