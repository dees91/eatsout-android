package pl.deesoft.eatsout.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by Piotr Krawczyk on 12.06.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class Opinion {

    @SerializedName("id")
    private long id;

    @SerializedName("rate")
    private float rate;

    @SerializedName("comment")
    private String comment;

    @SerializedName("restaurant")
    private Restaurant restaurant;

    @SerializedName("user")
    private User user;

    @SerializedName("created_at")
    private Date date;

    public Opinion() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
