package pl.deesoft.eatsout.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import hugo.weaving.DebugLog;

/**
 * Created by Piotr Krawczyk on 25.04.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class User {

    @SerializedName("id")
    private long id;

    @SerializedName("username")
    private String username;

    @SerializedName("plainPassword")
    private String password;

    @SerializedName("email")
    private String email;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("city")
    private String city;

    @SerializedName("country")
    private String country;

    @SerializedName("phone")
    private String phone;

    @SerializedName("phone_id")
    private String phone_id;

    @SerializedName("fb_token")
    private String fb_token;

    @SerializedName("avatar")
    private Avatar avatar;

    @SerializedName("following")
    private List<Follow> following;

    @SerializedName("opinions")
    private int opinions;

    @SerializedName("photos")
    private int photos;

    @SerializedName("followed")
    private int followed;

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone_id() {
        return phone_id;
    }

    public void setPhone_id(String phone_id) {
        this.phone_id = phone_id;
    }

    public String getFb_token() {
        return fb_token;
    }

    public void setFb_token(String fb_token) {
        this.fb_token = fb_token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public List<Follow> getFollowing() {
        return following;
    }

    public void setFollowing(List<Follow> following) {
        this.following = following;
    }

    public int getOpinions() {
        return opinions;
    }

    public void setOpinions(int opinions) {
        this.opinions = opinions;
    }

    public int getPhotos() {
        return photos;
    }

    public void setPhotos(int photos) {
        this.photos = photos;
    }

    public int getFollowed() {
        return followed;
    }

    public void setFollowed(int followed) {
        this.followed = followed;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @DebugLog
    public Follow getFollowByUser(User user) {
        if (following != null) {
            for (Follow follow : following) {
                if (follow != null && follow.getFollowed() != null && user != null && follow.getFollowed().getId() == user.getId()) {
                    return follow;
                }
            }
        }

        return null;
    }

    public List<User> getFollowingUsers() {
        List<User> followingUsers = new ArrayList<>();
        if (following != null) {
            for (Follow follow : following) {
                followingUsers.add(follow.getFollowing());
            }
        }
        return followingUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (getId() != user.getId()) return false;
        return getEmail().equals(user.getEmail());
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + getEmail().hashCode();
        return result;
    }
}
