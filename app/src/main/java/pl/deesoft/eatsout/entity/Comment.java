package pl.deesoft.eatsout.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 28.04.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class Comment {

    @SerializedName("id")
    private long id;

    @SerializedName("content")
    private String content;

    @SerializedName("photo")
    private Photo photo;

    @SerializedName("user")
    private User user;

    @SerializedName("parent")
    private Comment parent;

    public Comment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Comment getParent() {
        return parent;
    }

    public void setParent(Comment parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", photo=" + photo +
                ", user=" + user +
                ", parent=" + parent +
                '}';
    }
}
