package pl.deesoft.eatsout;

import android.app.Activity;
import android.os.Bundle;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import pl.deesoft.eatsout.event.ActivityFinishRequestEvent;
import pl.deesoft.eatsout.event.ApplicationReadyEvent;
import pl.deesoft.eatsout.util.Constants;

public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start);
        ButterKnife.inject(this);

        Hawk.init(this, Constants.HAWK_PASSWORD, new Hawk.Callback() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new ApplicationReadyEvent());
            }

            @Override
            public void onFail(Exception e) {
                Logger.e(e.getMessage());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onEvent(ActivityFinishRequestEvent activityFinishRequestEvent) {
        finish();
    }
}
