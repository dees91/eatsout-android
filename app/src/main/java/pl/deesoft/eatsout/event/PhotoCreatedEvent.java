package pl.deesoft.eatsout.event;

import pl.deesoft.eatsout.entity.Photo;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PhotoCreatedEvent {
    Photo photo;

    public PhotoCreatedEvent(Photo photo) {
        this.photo = photo;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }
}
