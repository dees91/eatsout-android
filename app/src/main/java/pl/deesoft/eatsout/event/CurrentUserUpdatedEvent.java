package pl.deesoft.eatsout.event;

import pl.deesoft.eatsout.entity.User;

/**
 * Created by Piotr Krawczyk on 13.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class CurrentUserUpdatedEvent {
    User user;

    public CurrentUserUpdatedEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
