package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import pl.deesoft.eatsout.api.model.Hash;

/**
 * Created by Piotr Krawczyk on 02.05.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class PasswordResettedEvent {

    @SerializedName("hash")
    Hash hash;

    public PasswordResettedEvent() {}

    public Hash getHash() {
        return hash;
    }

    public void setHash(Hash hash) {
        this.hash = hash;
    }
}
