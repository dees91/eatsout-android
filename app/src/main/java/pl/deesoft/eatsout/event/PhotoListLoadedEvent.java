package pl.deesoft.eatsout.event;

import java.util.List;

import pl.deesoft.eatsout.entity.Photo;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PhotoListLoadedEvent extends ResourceListLoadedEvent {
    private List<Photo> photos;

    public PhotoListLoadedEvent(List<Photo> photos, String cacheKey) {
        super(cacheKey);
        this.photos = photos;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }
}
