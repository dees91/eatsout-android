package pl.deesoft.eatsout.event;

import java.util.List;

import pl.deesoft.eatsout.entity.User;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class UserListLoadedEvent extends ResourceListLoadedEvent {
    private List<User> users;

    public UserListLoadedEvent(List<User> users, String cacheKey) {
        super(cacheKey);
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
