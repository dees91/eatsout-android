package pl.deesoft.eatsout.event;

/**
 * Created by Piotr Krawczyk on 13.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class FollowCreateRequestEvent {
    long following;
    long followed;

    public FollowCreateRequestEvent(long following, long followed) {
        this.following = following;
        this.followed = followed;
    }

    public long getFollowing() {
        return following;
    }

    public void setFollowing(long following) {
        this.following = following;
    }

    public long getFollowed() {
        return followed;
    }

    public void setFollowed(long followed) {
        this.followed = followed;
    }
}
