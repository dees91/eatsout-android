package pl.deesoft.eatsout.event;

import retrofit.mime.TypedFile;

/**
 * Created by Piotr Krawczyk on 05.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class AvatarUpdateRequestEvent {
    long id;
    long user;
    TypedFile file;

    public AvatarUpdateRequestEvent(long id, long user, TypedFile file) {
        this.id = id;
        this.user = user;
        this.file = file;
    }

    public AvatarUpdateRequestEvent(long user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public TypedFile getFile() {
        return file;
    }

    public void setFile(TypedFile file) {
        this.file = file;
    }
}
