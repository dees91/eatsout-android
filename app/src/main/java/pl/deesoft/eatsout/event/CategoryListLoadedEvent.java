package pl.deesoft.eatsout.event;

import java.util.List;

import pl.deesoft.eatsout.entity.Category;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class CategoryListLoadedEvent extends ResourceListLoadedEvent {
    private List<Category> categories;

    public CategoryListLoadedEvent(List<Category> categories, String cacheKey) {
        super(cacheKey);
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
