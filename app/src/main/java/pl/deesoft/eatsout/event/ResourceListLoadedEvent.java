package pl.deesoft.eatsout.event;

/**
 * Created by Piotr Krawczyk on 11.06.15.
 * krawczyk.piotr.91@gmail.com
 */
abstract public class ResourceListLoadedEvent {
    protected String cacheKey;

    public ResourceListLoadedEvent(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }
}
