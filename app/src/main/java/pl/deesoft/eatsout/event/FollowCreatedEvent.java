package pl.deesoft.eatsout.event;

import pl.deesoft.eatsout.entity.Follow;

/**
 * Created by Piotr Krawczyk on 13.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class FollowCreatedEvent {
    Follow follow;

    public FollowCreatedEvent(Follow follow) {
        this.follow = follow;
    }

    public Follow getFollow() {
        return follow;
    }

    public void setFollow(Follow follow) {
        this.follow = follow;
    }
}
