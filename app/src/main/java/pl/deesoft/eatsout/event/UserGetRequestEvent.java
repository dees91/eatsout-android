package pl.deesoft.eatsout.event;

/**
 * Created by Piotr Krawczyk on 13.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class UserGetRequestEvent {
    long id;
    boolean current;

    public UserGetRequestEvent(long id, boolean current) {
        this.id = id;
        this.current = current;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }
}
