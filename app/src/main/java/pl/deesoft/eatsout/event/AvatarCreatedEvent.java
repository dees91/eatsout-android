package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import pl.deesoft.eatsout.entity.User;

/**
 * Created by Piotr Krawczyk on 05.06.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class AvatarCreatedEvent {
    @SerializedName("id")
    long id;

    @SerializedName("user")
    User user;

    @SerializedName("file")
    String file;

    public AvatarCreatedEvent() {
    }

    public AvatarCreatedEvent(User user, String file) {
        this.user = user;
        this.file = file;
    }

    public AvatarCreatedEvent(long id, User user, String file) {
        this.id = id;
        this.user = user;
        this.file = file;
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
