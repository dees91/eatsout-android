package pl.deesoft.eatsout.event;

import java.util.List;

import pl.deesoft.eatsout.entity.Restaurant;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class RestaurantListLoadedEvent extends ResourceListLoadedEvent {
    private List<Restaurant> restaurants;

    public RestaurantListLoadedEvent(List<Restaurant> restaurants, String cacheKey) {
        super(cacheKey);
        this.restaurants = restaurants;
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }
}
