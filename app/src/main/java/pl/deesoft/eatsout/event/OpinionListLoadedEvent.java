package pl.deesoft.eatsout.event;

import java.util.List;

import pl.deesoft.eatsout.entity.Opinion;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class OpinionListLoadedEvent extends ResourceListLoadedEvent {
    private List<Opinion> opinions;

    public OpinionListLoadedEvent(List<Opinion> opinions, String cacheKey) {
        super(cacheKey);
        this.opinions = opinions;
    }

    public List<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(List<Opinion> opinions) {
        this.opinions = opinions;
    }
}
