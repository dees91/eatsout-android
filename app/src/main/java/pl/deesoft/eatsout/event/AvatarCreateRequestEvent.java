package pl.deesoft.eatsout.event;

import retrofit.mime.TypedFile;

/**
 * Created by Piotr Krawczyk on 05.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class AvatarCreateRequestEvent {
    long user;
    TypedFile file;

    public AvatarCreateRequestEvent(long user) {
        this.user = user;
    }

    public AvatarCreateRequestEvent(long user, TypedFile file) {
        this.user = user;
        this.file = file;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public TypedFile getFile() {
        return file;
    }

    public void setFile(TypedFile file) {
        this.file = file;
    }
}
