package pl.deesoft.eatsout.event;

import de.greenrobot.event.EventBus;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class RestaurantListRequestEvent extends ResourceListRequestEvent {
    public RestaurantListRequestEvent(String cacheKey) {
        super(cacheKey);
        EventBus.getDefault().post(new LocationUpdateRequestEvent());
    }
}
