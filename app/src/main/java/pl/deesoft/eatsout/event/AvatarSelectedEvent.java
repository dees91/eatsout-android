package pl.deesoft.eatsout.event;

import android.net.Uri;

/**
 * Created by Piotr Krawczyk on 15.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class AvatarSelectedEvent {
    Uri uri;

    public AvatarSelectedEvent(Uri uri) {
        this.uri = uri;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }
}
