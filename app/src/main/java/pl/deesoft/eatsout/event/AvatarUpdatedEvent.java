package pl.deesoft.eatsout.event;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 05.06.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class AvatarUpdatedEvent extends AvatarCreatedEvent {

}
