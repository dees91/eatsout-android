package pl.deesoft.eatsout.event;

import pl.deesoft.eatsout.entity.Restaurant;

/**
 * Created by Piotr Krawczyk on 23.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class RestaurantSelectedEvent {
    Restaurant restaurant;

    public RestaurantSelectedEvent(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
