package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 08.05.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class PasswordAuthenticationRequestEvent {

    @SerializedName("username")
    private String username;

    @SerializedName("plainPassword")
    private String password;

    public PasswordAuthenticationRequestEvent() {}

    public PasswordAuthenticationRequestEvent(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
