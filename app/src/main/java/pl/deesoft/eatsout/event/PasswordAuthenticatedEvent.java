package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import pl.deesoft.eatsout.entity.User;

/**
 * Created by Piotr Krawczyk on 27.04.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class PasswordAuthenticatedEvent {

    @SerializedName("user")
    private User user;

    @SerializedName("token")
    private String token;

    @SerializedName("registered")
    private boolean registered;


    public PasswordAuthenticatedEvent() {}

    public PasswordAuthenticatedEvent(User user, String token) {
        this.user = user;
        this.token = token;
    }

    public PasswordAuthenticatedEvent(User user, String token, boolean registered) {
        this.user = user;
        this.token = token;
        this.registered = registered;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
}
