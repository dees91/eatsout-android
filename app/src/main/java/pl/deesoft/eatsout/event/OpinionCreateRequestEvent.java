package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 12.06.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class OpinionCreateRequestEvent {
    @SerializedName("rate")
    float rate;
    @SerializedName("comment")
    String comment;
    @SerializedName("restaurant")
    long restaurant;
    @SerializedName("user")
    long user;

    public OpinionCreateRequestEvent() {
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(long restaurant) {
        this.restaurant = restaurant;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }
}
