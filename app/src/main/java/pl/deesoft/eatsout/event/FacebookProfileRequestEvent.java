package pl.deesoft.eatsout.event;

import com.facebook.AccessToken;

/**
 * Created by Piotr Krawczyk on 08.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class FacebookProfileRequestEvent {
    private AccessToken accessToken;

    public FacebookProfileRequestEvent(AccessToken accessToken) {
        this.accessToken = accessToken;
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
    }
}
