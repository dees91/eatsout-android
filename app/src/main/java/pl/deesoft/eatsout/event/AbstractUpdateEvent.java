package pl.deesoft.eatsout.event;

/**
 * Created by Piotr Krawczyk on 05.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public abstract class AbstractUpdateEvent {
    public interface UpdateEventBuilder {
        public AbstractUpdateEvent buildRequestEvent();
    }
}
