package pl.deesoft.eatsout.event;

/**
 * Created by Piotr Krawczyk on 15.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class PhotoCameraRequestEvent {
    int code;

    public PhotoCameraRequestEvent(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
