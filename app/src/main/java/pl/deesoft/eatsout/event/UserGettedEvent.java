package pl.deesoft.eatsout.event;

import pl.deesoft.eatsout.entity.User;

/**
 * Created by Piotr Krawczyk on 13.06.15.
 * krawczyk.piotr.91@gmail.com
 */
public class UserGettedEvent {
    User user;
    boolean current;

    public UserGettedEvent(User user, boolean current) {
        this.user = user;
        this.current = current;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
