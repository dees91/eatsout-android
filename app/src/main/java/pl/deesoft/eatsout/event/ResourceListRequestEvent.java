package pl.deesoft.eatsout.event;

import java.util.HashMap;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class ResourceListRequestEvent {

    public static final int RESOURCE_TAG = 1;
    public static final int RESOURCE_RESTAURANT = 2;
    public static final int RESOURCE_PHOTO = 3;
    public static final int RESOURCE_CATEGORY = 4;
    public static final int RESOURCE_USER = 5;
    public static final int RESOURCE_OPINION = 6;

    private int resource;

    private int page;

    private int limit;

    private boolean paginated;

    private HashMap<String, String> criteria;

    private String cacheKey = null;

    public ResourceListRequestEvent(int resource) {
        this.resource = resource;
        page = 1;
        limit = 10;
        paginated = false;
        criteria = new HashMap<>();
    }

    public ResourceListRequestEvent(int resource, String cacheKey) {
        this.resource = resource;
        page = 1;
        limit = 10;
        paginated = false;
        criteria = new HashMap<>();
        this.cacheKey = cacheKey;
    }

    public ResourceListRequestEvent(String cacheKey) {
        this.resource = -1;
        page = 1;
        limit = 10;
        paginated = false;
        criteria = new HashMap<>();
        this.cacheKey = cacheKey;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public boolean isPaginated() {
        return paginated;
    }

    public void setPaginated(boolean paginated) {
        this.paginated = paginated;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public void addCriterium(String key, String value) {
        String parsed = "criteria";
        String[] parts = key.split("\\.");

        for (String part : parts) {
            String glued = "[" + part + "]";
            parsed += glued;
        }

        criteria.put(parsed, value);
    }

    public HashMap<String, String> getCriteria() {
        return criteria;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }
}
