package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 08.05.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class FacebookAuthenticationRequestEvent {

    @SerializedName("fb_token")
    private String facebookToken;

    @SerializedName("email")
    private String email;

    public FacebookAuthenticationRequestEvent(){}

    public FacebookAuthenticationRequestEvent(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public FacebookAuthenticationRequestEvent(String facebookToken, String email) {
        this.facebookToken = facebookToken;
        this.email = email;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
