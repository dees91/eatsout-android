package pl.deesoft.eatsout.event;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class ActivityRedirectEvent {

    private Class<?> cls;

    public ActivityRedirectEvent(Class<?> cls) {
        this.cls = cls;
    }

    public Class<?> getCls() {
        return cls;
    }

    public void setCls(Class<?> cls) {
        this.cls = cls;
    }
}
