package pl.deesoft.eatsout.event;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Piotr Krawczyk on 24.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class LocationUpdatedEvent {
    LatLng latLng;

    public LocationUpdatedEvent(LatLng latLng) {
        this.latLng = latLng;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
