package pl.deesoft.eatsout.event;

import java.util.List;

import pl.deesoft.eatsout.entity.Tag;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class TagListLoadedEvent extends ResourceListLoadedEvent {
    private List<Tag> tags;

    public TagListLoadedEvent(List<Tag> tags, String cacheKey) {
        super(cacheKey);
        this.tags = tags;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
