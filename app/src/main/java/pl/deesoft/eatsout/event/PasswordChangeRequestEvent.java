package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Piotr Krawczyk on 08.05.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class PasswordChangeRequestEvent {
    @SerializedName("username")
    private String email;

    @SerializedName("hash")
    private String hash;

    @SerializedName("plainPassword")
    private String password;

    public PasswordChangeRequestEvent() {}

    public PasswordChangeRequestEvent(String email, String hash, String password) {
        this.email = email;
        this.hash = hash;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
