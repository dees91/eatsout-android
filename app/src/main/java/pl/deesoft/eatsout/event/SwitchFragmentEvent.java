package pl.deesoft.eatsout.event;

import pl.deesoft.eatsout.fragment.BaseFragment;

/**
 * Created by Piotr Krawczyk on 05.05.15.
 * krawczyk.piotr.91@gmail.com
 */
public class SwitchFragmentEvent {
    BaseFragment baseFragment;
    boolean force;

    public SwitchFragmentEvent(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }

    public SwitchFragmentEvent(BaseFragment baseFragment, boolean force) {
        this.baseFragment = baseFragment;
        this.force = force;
    }

    public BaseFragment getBaseFragment() {
        return baseFragment;
    }

    public void setBaseFragment(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }
}
