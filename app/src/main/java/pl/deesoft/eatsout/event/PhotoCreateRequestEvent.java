package pl.deesoft.eatsout.event;

import java.util.List;

import pl.deesoft.eatsout.entity.Tag;
import retrofit.mime.TypedFile;

/**
 * Created by Piotr Krawczyk on 11.05.15.
 * krawczyk.piotr.91@gmail.com
 */

public class PhotoCreateRequestEvent extends AbstractUpdateEvent {

    String title;

    String description;

    long user;

    List<Tag> tags;

    long restaurant;

    TypedFile file;

    public PhotoCreateRequestEvent() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public long getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(long restaurant) {
        this.restaurant = restaurant;
    }

    public TypedFile getFile() {
        return file;
    }

    public void setFile(TypedFile file) {
        this.file = file;
    }
}
