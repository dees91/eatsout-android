package pl.deesoft.eatsout.event;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import pl.deesoft.eatsout.entity.User;

/**
 * Created by Piotr Krawczyk on 08.05.15.
 * krawczyk.piotr.91@gmail.com
 */
@Parcel
public class RegisteredEvent {

    @SerializedName("user")
    private User user;

    public RegisteredEvent() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
